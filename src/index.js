import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import registerServiceWorker from './registerServiceWorker';

import App from './App';
import './css/app.css';

import store from './store'
import { connect, call } from './reducers/client-api'

store.dispatch(connect('ws://207.148.65.3:8080/v1/json/socket', true));
setInterval(() => { 
  if (store.getState().account.sessionId.length > 0) store.dispatch(call('ping', { sessionId: store.getState().account.sessionId })) 
}, 300000)
ReactDOM.render(<Provider store={store} ><App /></Provider>, document.getElementById('root'));
// registerServiceWorker();
