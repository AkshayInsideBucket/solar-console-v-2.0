const calculateHealth = readings => {
  if (readings.length === 0) return 0
  else {
    const invertersAlive = readings.reduce((prev, curr) => curr.health ? prev + 1 : prev, 0)
    return (invertersAlive / readings.length) * 100
  }
}

export default calculateHealth