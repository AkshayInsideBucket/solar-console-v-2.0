import React, { Component } from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import Notifications from 'react-notification-system-redux';
import history from './history'

import LoginContainer from './components/login/login-container'
import AdminHomeContainer from './components/admin/admin-home/admin-home-container'
import AdminSiteContainer from './components/admin/admin-site/admin-site-container'
import AdminInverterContainer from './components/admin/admin-inverter/admin-inverter-container'
import ClientWrapperContainer from './components/client/client-wrapper-container'

const NotificationComponent = (props) => {
  return (<Notifications notifications={props.notifications} />);
};

const NotificationContainer = connect(state => ({ notifications: state.notifications }))(NotificationComponent)

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <div>
          <NotificationContainer />
          <Route exact path="/" component={LoginContainer} />
          <Route exact path="/admin" component={AdminHomeContainer} />
          <Route exact path="/admin/:account" component={AdminSiteContainer} />
          <Route exact path="/admin/:account/:siteId" component={AdminInverterContainer} />
          <Route path='/client' component={ClientWrapperContainer} />
        </div>
      </Router>
    );
  }
}

export default App;
