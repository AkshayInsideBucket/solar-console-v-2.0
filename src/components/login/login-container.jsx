import { connect } from 'react-redux'
import Login from './login'
import { call } from '../../reducers/client-api'

const mapDispatchToProps = (dispatch) => {
  return {
    login: (account, pass) => {
      dispatch(call('login', { account: account, pass: pass }))
    }
  }
}

const mapStateToProps = state => {
  return {

  }
}

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login)

export default LoginContainer
