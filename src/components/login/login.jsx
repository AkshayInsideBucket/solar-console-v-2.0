import React from 'react'
import LoginForm from './login-form'

const Login = (props) => {
  return (
    <div className="login-page">
      <div className="background-image"></div>
      <LoginForm login={props.login}/>
    </div>
  )
}

export default Login