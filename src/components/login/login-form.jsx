import React from 'react'
import { Row, Col, Card, Form, Icon, Input, Button } from 'antd';
var wrapperImage = require('../../images/login-card-bg.jpg')
const FormItem = Form.Item;

class LoginFormContent extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.login(values.account, values.password)
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-form-content">
        <Row>
          <Col xs={{ span: 24, offset: 0 }} lg={{ span: 16, offset: 4 }} xl={{ span: 12, offset: 6 }}>
            <Card bordered={false}>
              <div className="wrapper-container">
                <div className="wrapper-image">
                  <img src={wrapperImage} />
                  <div className="image-text">
                    <p className="hello-text">Hello.</p>
                    <p className="welcome-text">Welcome to Solar Console.</p>
                    <p className="get-started-text">Please login so we can get started.</p>
                  </div>
                </div>
                <div className="wrapper-content">
                  <Col xs={{ span: 20, offset: 2 }}>
                    <div className="login-header">
                      <p>Login</p>
                      <hr/>
                    </div>
                    <Form onSubmit={this.handleSubmit} className="login-form">
                      <FormItem>
                        {getFieldDecorator('account', {
                          rules: [{ required: true, message: 'Please input your username!' }],
                        })(
                          <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Account" />
                          )}
                      </FormItem>
                      <FormItem>
                        {getFieldDecorator('password', {
                          rules: [{ required: true, message: 'Please input your Password!' }],
                        })(
                          <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                          )}
                      </FormItem>
                      <FormItem>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                          Log in
                        </Button>
                      </FormItem>
                    </Form>
                  </Col>
                </div>
              </div>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

const LoginForm = Form.create()(LoginFormContent);

export default LoginForm