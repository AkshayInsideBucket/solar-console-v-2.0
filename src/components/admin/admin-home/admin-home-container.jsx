import { connect } from 'react-redux'
import store from '../../../store'
import history from '../../../history'
import { call } from '../../../reducers/client-api'
import AdminHome from './admin-home'

const mapDispatchToProps = (dispatch) => {
  return {
    onMount: () => {
      const state = store.getState()
      dispatch(call('fetch-readings', { sessionId: state.account.sessionId }))
    },
    logout: () => {
      history.replace('/')
      dispatch({ type: 'logout' })
    },
    delete: (id) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('remove-user', { sessionId: sessionId, account: id }))
    },
    addUser: (name, userName, email, pass, mob) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('add-user', { sessionId: sessionId, account: userName, name: name, pass: pass, email: email, contact: mob }))
    },
    editUser: (id, name, userName, email, pass, mob) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('update-user', { sessionId: sessionId, account: userName, name: name, pass: pass, email: email, contact: mob }))
    }
  }
}

const mapStateToProps = state => {
  return {
    homeData: state.admin.accounts.map(obj => { return { name: obj.name, userName: obj.account, pass: obj.pass, email: obj.email, mob: obj.contact } })
  }
}

const AdminHomeContainer = connect(mapStateToProps, mapDispatchToProps)(AdminHome)

export default AdminHomeContainer
