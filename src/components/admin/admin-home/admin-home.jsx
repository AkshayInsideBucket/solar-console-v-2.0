import React from 'react'
import history from '../../../history'
import { Row, Col, Button, Table, Icon, Divider, Popconfirm, Form } from 'antd';

import AddUserForm from './add-user'

class AdminHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      modalTitle: false,
      userInfo: '',
      userId: ''
    };
    this.onAddUser = this.onAddUser.bind(this);
    this.onEditUser = this.onEditUser.bind(this);
  }
  
  componentDidMount() {
    this.props.onMount()
  }

  onAddUser = () => {
    this.setState({
      visible: true,
      modalTitle: true,
      userInfo: '',
      userId: ''
    });
  }

  onEditUser = (e) => {
    this.setState({
      visible: true,
      modalTitle: false,
      userInfo: e,
      userId: e.id
    });
  }

  handleCancel = () => {
    this.setState({ visible: false });
    const form = this.form;
    form.resetFields();
  }

  handleSave = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (this.state.modalTitle == true) {
        this.props.addUser(values.name, values.userName, values.email, values.pass, values.mob)
      }
      if (this.state.modalTitle == false) {
        this.props.editUser(this.state.userId, values.name, values.userName, values.email, values.pass, values.mob)
      }
      form.resetFields();
      this.setState({
        visible: false
      });
    });
  }

  saveFormRef = (form) => {
    this.form = form;
  }

  render() {
    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    }, {
      title: 'Username',
      dataIndex: 'userName',
      key: 'userName'
    }, {
      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    }, {
      title: 'Password',
      dataIndex: 'pass',
      key: 'pass',
    }, {
      title: 'Mobile No.',
      dataIndex: 'mob',
      key: 'mob'
    }, {
      title: 'Actions',
      key: 'action',
      dataIndex: 'action',
      render: (text, record) => (
        <span>
          <a onClick={() => this.onEditUser(record)}>Edit</a>
          <Divider type="vertical" />
          <a onClick={() => history.push('/admin/' + record.userName)}>View</a>
          <Divider type="vertical" />
          <Popconfirm onConfirm={() => this.props.delete(record.userName)} title="Are you sure？" okText="Yes" cancelText="No">
            <a>Delete</a>
          </Popconfirm>
        </span>
      ),
    }];

    return (
      <div className="admin-page">
        <div className="topbar">
          <p>Admin | Home</p>
          <Button onClick={this.props.logout} type="primary" icon="logout">Logout</Button>
        </div>
        <div className="content">
          <Row>
            <Col xs={{ span: 20, offset: 2 }} md={{ span: 22, offset: 1 }} lg={{ span: 20, offset: 2 }} >
              <Button onClick={this.onAddUser} icon="user-add">New User</Button>
              <AddUserForm
                ref={this.saveFormRef}
                visible={this.state.visible}
                onCancel={this.handleCancel}
                onSave={this.handleSave}
                modalTitle={this.state.modalTitle}
                userInfo={this.state.userInfo}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 22, offset: 1 }} lg={{ span: 20, offset: 2 }}>
              <Table pagination={{ defaultPageSize: 6 }} columns={columns} dataSource={this.props.homeData} />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default AdminHome