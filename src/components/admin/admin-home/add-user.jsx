import React from 'react'
import { Button, Modal, Form, Input } from 'antd';
const FormItem = Form.Item;

const AddUserForm = Form.create()(
  (props) => {
    const { visible, onCancel, onSave, form, modalTitle, userInfo } = props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        visible={visible}
        title={modalTitle == true ? 'Add User' : 'Edit User'}
        okText="Save"
        onCancel={onCancel}
        onOk={onSave}
      >
        <Form layout="vertical">
          <FormItem label="Name">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input your Name!' }],
              initialValue: userInfo.name
            })(
              <Input />
              )}
          </FormItem>
          <FormItem label="Username">
            {getFieldDecorator('userName', {
              rules: [{ required: true, message: 'Please input your Username!' }],
              initialValue: userInfo.userName
            })(
              <Input disabled={!modalTitle} />
              )}
          </FormItem>
          <FormItem label="Email">
            {getFieldDecorator('email', {
              rules: [{
                type: 'email', message: 'The input is not valid E-mail!',
              }, {
                required: true, message: 'Please input your E-mail!',
              }],
              initialValue: userInfo.email
            })(
              <Input />
              )}
          </FormItem>
          <FormItem label="Password">
            {getFieldDecorator('pass', {
              rules: [{ required: true, message: 'Please input your Password!' }],
              initialValue: userInfo.pass
            })(
              <Input />
              )}
          </FormItem>
          <FormItem label="Mobile No.">
            {getFieldDecorator('mob', {
              rules: [{ required: true, message: 'Please input your Mobile No.!' }],
              initialValue: userInfo.mob
            })(
              <Input type={'number'} />
              )}
          </FormItem>
        </Form>
      </Modal>
    )
  })

export default AddUserForm