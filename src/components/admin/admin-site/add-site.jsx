import React from 'react'
import { Button, Modal, Form, Input } from 'antd';
const FormItem = Form.Item;

const AddSiteForm = Form.create()(
  (props) => {
    const { visible, onCancel, onSave, form, modalTitle, siteInfo } = props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        visible={visible}
        title={modalTitle == true ? 'Add Site' : 'Edit Site'}
        okText="Save"
        onCancel={onCancel}
        onOk={onSave}
      >
        <Form layout="vertical">
          <FormItem label="Site Name">
            {getFieldDecorator('siteName', {
              rules: [{ required: true, message: 'Please input your Site Name!' }],
              initialValue: siteInfo.siteName
            })(
              <Input />
              )}
          </FormItem>
          <FormItem label="Site ID">
            {getFieldDecorator('siteId', {
              rules: [{ required: true, message: 'Please input your Site ID!' }],
              initialValue: siteInfo.siteId
            })(
              <Input disabled={!modalTitle} />
              )}
          </FormItem>
          <FormItem label="Location">
            {getFieldDecorator('location', {
              rules: [{ required: true, message: 'Please input your Location!' }],
              initialValue: siteInfo.location
            })(
              <Input />
              )}
          </FormItem>
          <FormItem label="Rate">
            {getFieldDecorator('rate', {
              rules: [{ required: true, message: 'Please input your Rate!' }],
              initialValue: siteInfo.rate
            })(
              <Input type={'number'} />
              )}
          </FormItem>
        </Form>
      </Modal>
    )
  })

export default AddSiteForm