import React from 'react'
import history from '../../../history'
import { Row, Col, Button, Table, Icon, Divider, Popconfirm, Form } from 'antd';

import AddSiteForm from './add-site'

class AdminSite extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      modalTitle: false,
      siteInfo: '',
      siteId: ''
    };
    this.onAddSite = this.onAddSite.bind(this);
    this.onEditSite = this.onEditSite.bind(this);
  }
  
  componentDidMount() {
    this.props.onMount()
  }

  onAddSite = () => {
    this.setState({
      visible: true,
      modalTitle: true,
      siteInfo: '',
      siteId: ''
    });
  }

  onEditSite = (e) => {
    this.setState({
      visible: true,
      modalTitle: false,
      siteInfo: e,
      siteId: e.id
    });
  }

  handleCancel = () => {
    const form = this.form;
    form.resetFields();
    this.setState({ visible: false });
  }

  handleSave = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (this.state.modalTitle == true) {
        this.props.addSite(values.siteName, values.siteId, values.location, values.rate, this.props.match.params.account)
      }
      if (this.state.modalTitle == false) {
        this.props.editSite(this.state.siteId, values.siteName, values.siteId, values.location, values.rate)
      }
      form.resetFields();
      this.setState({ visible: false });
    });
  }

  saveFormRef = (form) => {
    this.form = form;
  }

  render() {
    const columns = [{
      title: 'Site ID',
      dataIndex: 'siteId',
      key: 'siteId'
    }, {
      title: 'Site Name',
      dataIndex: 'siteName',
      key: 'siteName'
    }, {
      title: 'Location',
      dataIndex: 'location',
      key: 'location'
    }, {
      title: 'Rate',
      dataIndex: 'rate',
      key: 'rate',
    }, {
      title: 'Actions',
      key: 'action',
      dataIndex: 'action',
      render: (text, record) => (
        <span>
          <a onClick={() => this.onEditSite(record)}>Edit</a>
          <Divider type="vertical" />
          <a onClick={() => history.push('/admin/' + this.props.match.params.account + '/' + record.siteId)}>View</a>
          <Divider type="vertical" />
          <Popconfirm onConfirm={() => this.props.delete(record.siteId)} title="Are you sure？" okText="Yes" cancelText="No">
            <a>Delete</a>
          </Popconfirm>
        </span>
      ),
    }];

    return (
      <div className="admin-page">
        <div className="topbar">
          <span className="icon-wrapper">
            <i onClick={() => history.goBack()} className="material-icons arrow-icon">arrow_back</i>
          </span>
          <p>Admin | Site</p>
          <Button onClick={this.props.logout} type="primary" icon="logout">Logout</Button>
        </div>
        <div className="content">
          <Row>
            <Col xs={{ span: 20, offset: 2 }} md={{ span: 22, offset: 1 }} lg={{ span: 20, offset: 2 }} >
              <Button onClick={this.onAddSite} icon="home">New Site</Button>
              <AddSiteForm
                ref={this.saveFormRef}
                visible={this.state.visible}
                onCancel={this.handleCancel}
                onSave={this.handleSave}
                modalTitle={this.state.modalTitle}
                siteInfo={this.state.siteInfo}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 22, offset: 1 }} lg={{ span: 20, offset: 2 }}>
              <Table pagination={{ defaultPageSize: 6 }} columns={columns} dataSource={this.props.siteData} />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default AdminSite