import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import store from '../../../store'
import history from '../../../history'
import { call } from '../../../reducers/client-api'
import AdminSite from './admin-site'

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onMount: () => {
      dispatch(call('fetch-sites', { sessionId: store.getState().account.sessionId, account: ownProps.match.params.account }))
    },
    logout: () => {
      history.replace('/')
      dispatch({ type: 'logout' })
    },
    delete: (id) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('remove-site', { sessionId: sessionId, siteId: id }))
    },
    addSite: (siteName, siteId, location, rate, account) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('add-site', { sessionId: sessionId, account: account, siteId: siteId, siteName: siteName, location: location, rate: rate }))
    },
    editSite: (id, siteName, siteId, location, rate, account) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('update-site', { sessionId: sessionId, account: account, name: siteName, location: location, rate: rate, siteId: siteId }))
    }
  }
}

const mapStateToProps = state => {
  return {
    siteData: state.admin.sites.map(obj => { return { siteId: obj.site_id, siteName: obj.name, location: obj.location, rate: obj.rate } })
  }
}

const AdminSiteContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminSite))

export default AdminSiteContainer
