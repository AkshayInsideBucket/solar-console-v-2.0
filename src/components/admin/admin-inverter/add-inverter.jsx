import React from 'react'
import { Button, Modal, Form, Input, Select } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

const AddInverterForm = Form.create()(
  (props) => {
    const { visible, onCancel, onSave, form, modalTitle, inverterInfo } = props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        visible={visible}
        title={modalTitle == true ? 'Add Inverter' : 'Edit Inverter'}
        okText="Save"
        onCancel={onCancel}
        onOk={onSave}
      >
        <Form layout="vertical">
          <FormItem label="Inverter ID">
            {getFieldDecorator('inverterId', {
              rules: [{ required: true, message: 'Please input your Inverter ID!' }],
              initialValue: inverterInfo.inverterId
            })(
              <Input disabled={!modalTitle} />
              )}
          </FormItem>
          <FormItem label="Capacity">
            {getFieldDecorator('capacity', {
              rules: [{ required: true, message: 'Please input your Capacity!' }],
              initialValue: inverterInfo.capacity
            })(
              <Input type={'number'} />
              )}
          </FormItem>
          <FormItem label="Mppt Levels">
            {getFieldDecorator('mpptLevel', {
              rules: [{ required: true, message: 'Please input your Mppt levels!' }],
              initialValue: inverterInfo.mpptLevel
            })(
              <Select placeholder="Please select a Mppt Level">
                <Option value="1">1</Option>
                <Option value="2">2</Option>
                <Option value="3">3</Option>
              </Select>
              )}
          </FormItem>
        </Form>
      </Modal>
    )
  })

export default AddInverterForm