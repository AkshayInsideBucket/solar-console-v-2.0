import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import store from '../../../store'
import history from '../../../history'
import { call } from '../../../reducers/client-api'
import AdminInverter from './admin-inverter'


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onMount: () => {
      dispatch(call('fetch-inverters', { sessionId: store.getState().account.sessionId, siteId: ownProps.match.params.siteId }))
    },
    logout: () => {
      history.replace('/')
      dispatch({ type: 'logout' })
    },
    delete: (id, siteId) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('remove-inverter', { sessionId: sessionId, siteId: siteId, inverterId: id }))
    },
    addInverter: (inverterId, capacity, mpptLevel, siteId, account) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('add-inverter', { sessionId: sessionId, account: account, siteId: siteId, inverterId: inverterId, capacity: capacity, levels: mpptLevel }))

    },
    editInverter: (id, inverterId, capacity, mpptLevel, siteId) => {
      let sessionId = store.getState().account.sessionId
      dispatch(call('update-inverter', { sessionId: sessionId, inverterId: inverterId, capacity: capacity, mppt: mpptLevel, siteId: siteId }))
    }
  }
}

const mapStateToProps = state => {
  return {
    inverterData: state.admin.inverters.map(obj => { return { inverterId: obj.inverter_id, capacity: obj.capacity, mpptLevel: obj.mppt } })
  }
}

const AdminInverterContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminInverter))

export default AdminInverterContainer
