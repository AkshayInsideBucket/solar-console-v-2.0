import React from 'react'
import history from '../../../history'
import { Row, Col, Button, Table, Icon, Divider, Popconfirm, Form } from 'antd';

import AddInverterForm from './add-inverter'

class AdminInverter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      modalTitle: false,
      inverterInfo: '',
      inverterId: ''
    };
    this.onAddInverter = this.onAddInverter.bind(this);
    this.onEditInverter = this.onEditInverter.bind(this);
  }

  componentDidMount() {
    this.props.onMount()
  }

  onAddInverter = () => {
    this.setState({
      visible: true,
      modalTitle: true,
      inverterInfo: '',
      inverterId: ''
    });
  }

  onEditInverter = (e) => {
    this.setState({
      visible: true,
      modalTitle: false,
      inverterInfo: e,
      inverterId: e.id
    });
  }

  handleCancel = () => {
    const form = this.form;
    form.resetFields();
    this.setState({ visible: false });
  }

  handleSave = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (this.state.modalTitle == true) {
        this.props.addInverter(values.inverterId, values.capacity, values.mpptLevel, this.props.match.params.siteId, this.props.match.params.account)
      }
      if (this.state.modalTitle == false) {
        this.props.editInverter(this.state.inverterId, values.inverterId, values.capacity, values.mpptLevel, this.props.match.params.siteId)
      }
      form.resetFields();
      this.setState({ visible: false });
    });
  }

  saveFormRef = (form) => {
    this.form = form;
  }

  render() {
    const columns = [{
      title: 'Inverter ID',
      dataIndex: 'inverterId',
      key: 'inverterId'
    }, {
      title: 'Capacity',
      dataIndex: 'capacity',
      key: 'capacity'
    },{
      title: 'MPPT Levels',
      dataIndex: 'mpptLevel',
      key: 'mpptLevel'
    }, {
      title: 'Actions',
      key: 'action',
      dataIndex: 'action',
      render: (text, record) => (
        <span>
          <a onClick={() => this.onEditInverter(record)}>Edit</a>
          <Divider type="vertical" />
          <Popconfirm onConfirm={() => this.props.delete(record.inverterId, this.props.match.params.siteId)} title="Are you sure？" okText="Yes" cancelText="No">
            <a>Delete</a>
          </Popconfirm>
        </span>
      ),
    }];

    return (
      <div className="admin-page">
        <div className="topbar">
          <p>Admin | Inverter</p>
          <span className="icon-wrapper">
            <i onClick={() => history.goBack()} className="material-icons arrow-icon">arrow_back</i>
          </span>
          <Button onClick={this.props.logout} type="primary" icon="logout">Logout</Button>
        </div>
        <div className="content">
          <Row>
            <Col xs={{ span: 20, offset: 2 }} md={{ span: 22, offset: 1 }} lg={{ span: 20, offset: 2 }} >
              <Button onClick={this.onAddInverter} icon="dashboard">New Inverter</Button>
              <AddInverterForm
                ref={this.saveFormRef}
                visible={this.state.visible}
                onCancel={this.handleCancel}
                onSave={this.handleSave}
                modalTitle={this.state.modalTitle}
                inverterInfo={this.state.inverterInfo}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 22, offset: 1 }} lg={{ span: 20, offset: 2 }}>
              <Table pagination={{ defaultPageSize: 6 }} columns={columns} dataSource={this.props.inverterData} />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default AdminInverter