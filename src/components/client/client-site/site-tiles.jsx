import React from 'react'
import { Row, Col, Card } from 'antd'

const SiteTiles = (props) => {
  return (
    <div className="tiles">
      <Row>
        <Col xs={{ span: 24, offset: 0 }} md={{ span: 20, offset: 2 }}>
          <div className="flex-container">
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons business-icon site">business</i>
                    <div className="tiles-content">
                      <p className="value">{props.siteTiles.siteName}</p>
                      <p className="description">Site Name</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons power-icon site">power</i>
                    <div className="tiles-content">
                      <p className="value">{(props.siteTiles.currentPower / 1000) + ' kW'}</p>
                      <p className="description">Current Power</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons show_chart-icon site">show_chart</i>
                    <div className="tiles-content">
                      <p className="value">{(props.siteTiles.health) + '%'}</p>
                      <p className="description">Health</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons account_balance_wallet-icon site">account_balance_wallet</i>
                    <div className="tiles-content">
                      <p className="value">{'Rs ' + (props.siteTiles.income)}</p>
                      <p className="description">Income</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="tiles-content">
                      <p className="value">{props.siteTiles.todayEnergy + ' kWh'}</p>
                      <p className="description">Today's Energy</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="tiles-content">
                      <p className="value">{(props.siteTiles.monthlyEnergy) + ' kWh'}</p>
                      <p className="description">Monthly Energy</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="tiles-content">
                      <p className="value">{(props.siteTiles.annualEnergy) + ' kWh'}</p>
                      <p className="description">Annual Energy</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="tiles-content">
                      <p className="value">{(props.siteTiles.totalEnergy) + ' kWh'}</p>
                      <p className="description">Total Energy</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default SiteTiles