import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import store from '../../../store'
import history from '../../../history'
import { call } from '../../../reducers/client-api'
import calculateHealth from "../../../utils/health";
import ClientSite from './client-site'

const getInverters = (readings, siteId) => {
  return readings.filter((reading) => reading.site_id === siteId).map(obj => { return { siteName: obj.name, inverterId: obj.inverter_id, currentPower: obj.apw, peakPower: obj.pkw, todayEnergy: obj.etd, monthlyEnergy: obj.myd, annualEnergy: obj.ayd, totalEnergy: obj.tyd, time: obj.time, todayIncome: obj.etd * obj.rate, health: obj.health } })
}

const sensorReadings = [
  {
    pyra: null,
    temp: 320320,
    wind: 0
  }
]

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    logout: () => {
      history.replace('/')
      dispatch({ type: 'logout' })
    },
    selectInverter: (siteId, inverterId) => {
      history.push('/client/sites/' + siteId + '/' + inverterId)
    },
    onMount: () => {
      let state = store.getState()
      let date = new Date()
      let dateString = date.getFullYear() + '-' + (date.getMonth() + 1) + '-01'
      dispatch(call('fetch-site-energy-chart', { sessionId: state.account.sessionId, siteId: ownProps.match.params.siteId, firstDateOfMonth: dateString }))
      dispatch(call('fetch-readings', { sessionId: state.account.sessionId }))
      dispatch(call('fetch-sensor-readings', { sessionId: state.account.sessionId, siteId: ownProps.match.params.siteId }))
    },
    energySet: (month, year) => {
      let state = store.getState()
      dispatch(call('fetch-site-energy-chart', { sessionId: state.account.sessionId, siteId: ownProps.match.params.siteId, firstDateOfMonth: year + '-' + month + '-01' }))
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accountName: state.account.name,
    inverterCards: getInverters(state.client.readings, ownProps.match.params.siteId),
    siteTiles: Object.assign({}, getInverters(state.client.readings, ownProps.match.params.siteId).reduce((prev, curr) => {
      return {
        siteName: curr.siteName,
        inverters: prev.inverters + 1,
        currentPower: (((new Date(new Date().toString())) - (new Date(curr.time))) > 1800000) ? prev.currentPower : prev.currentPower + curr.currentPower,
        todayEnergy: prev.todayEnergy + curr.todayEnergy,
        monthlyEnergy: prev.monthlyEnergy + curr.monthlyEnergy,
        annualEnergy: prev.annualEnergy + curr.annualEnergy,
        totalEnergy: prev.totalEnergy + curr.totalEnergy,
        income: prev.income + curr.todayIncome
      }
    }, { siteName: '', inverters: 0, income: 0, todayEnergy: 0, currentPower: 0, monthlyEnergy: 0, annualEnergy: 0, totalEnergy: 0 }), { health: calculateHealth(getInverters(state.client.readings, ownProps.match.params.siteId)) }),
    energyChartData: state.client.charts.siteEnergy,
    sensorReadings: state.client.sensorReadings.map(obj => { return { pyra: obj.pm, temp: obj.tm, wind: obj.wm}})
  }
}

const ClientSiteContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientSite))

export default ClientSiteContainer
