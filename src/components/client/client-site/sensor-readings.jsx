import React from 'react'
import { Row, Col, Table } from 'antd'

class SensorReadings extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    let columns = [
      {
        title: 'Pyranometer Reading (W/m2)',
        dataIndex: 'pyra',
        key: 'pyra',
        render: text => (
          <span>
            {
              ((text === null) ? '-' : text)
            }
          </span>
        )
      },
      {
        title: 'Temperature (deg Celcius)',
        dataIndex: 'temp',
        key: 'temp',
        render: text => (
          <span>
            {
              ((text === null) ? '-' : text)
            }
          </span>
        )
      },
      {
        title: 'Wind Speed (m/s)',
        dataIndex: 'wind',
        key: 'wind',
        render: text => (
          <span>
            {
              ((text === null) ? '-' : text)
            }
          </span>
        )
      }
    ]

    return (
      <div className="sensor-readings">
        <Row>
          <Col xs={{ span: 20, offset: 2 }}>
            <p className="title">Sensor Readings :</p>
            <Table columns={columns} pagination={{ hideOnSinglePage: true }} dataSource={this.props.sensorReadings} />
          </Col>
        </Row>
      </div>
    )
  }

}

export default SensorReadings