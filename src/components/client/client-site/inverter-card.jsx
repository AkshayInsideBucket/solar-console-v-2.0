import React from 'react'
import { Row, Col, Card, Progress } from 'antd'

class InverterCard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timedOut: false
    }
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  tick() {
    const currentTime = new Date(new Date().toString())
    const lastTimeStamp = new Date(this.props.card.time)
    if ((currentTime - lastTimeStamp) > 1800000) this.setState({ timedOut: true })
    else this.setState({ timedOut: false })
  }

  render() {
    var inputDate = new Date(Date.parse(this.props.card.time) - (1000*60*330))
    var todaysDate = new Date()
    
    const isDateToday = inputDate.setHours(0,0,0,0) === todaysDate.setHours(0,0,0,0)
    return (
      <Col className="cards-column" xs={{ span: 24, offset: 0 }} md={{ span: 12, offset: 0 }} xl={{ span: 8, offset: 0 }}>
        <Card bordered={false}>
          <div onClick={this.props.selectInverter}>
            <p className="card-title">{'Inverter - ' + (this.props.card.inverterId)}</p>
            <div className="progress-bar inverter">
              <Progress type="circle" percent={99} format={percent => `${(this.state.timedOut) ? '--' : ((this.props.card.currentPower / 1000) + ' kWh')}`} />
              <p className="progress-text inverter">Current Power</p>
            </div>
            <div className="card-footer">
              <Row>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile  corner-tile">
                    <i className="material-icons power-icon site">power</i>
                    <div className="flex-text">
                      <p className="value">{ (!isDateToday) ? '--' : (this.props.card.peakPower / 1000) + ' kW'}</p>
                      <p className="description inverter">Today's Peak Power</p>
                    </div>
                  </div>
                </Col>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile">
                    <i className="material-icons account_balance_wallet-icon site">account_balance_wallet</i>
                    <div className="flex-text">
                      <p className="value">{'Rs ' + (this.props.card.todayIncome)}</p>
                      <p className="description inverter">Today's Income</p>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile corner-tile">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{(!isDateToday) ? '--' : ((this.props.card.todayEnergy) + ' kWh')}</p>
                      <p className="description inverter">Today's Energy</p>
                    </div>
                  </div>
                </Col>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{(this.props.card.monthlyEnergy) + ' kWh'}</p>
                      <p className="description inverter">Monthly Energy</p>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile corner-tile">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{this.props.card.annualEnergy !== null ? (this.props.card.annualEnergy) + ' kWh': '0 kWh'}</p>
                      <p className="description inverter">Annual Energy</p>
                    </div>
                  </div>
                </Col>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile">
                    <i className="material-icons flash_on-icon site">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{this.props.card.totalEnergy !== null ? (this.props.card.totalEnergy) + ' kWh' : '0 kWh'}</p>
                      <p className="description inverter">Total Energy</p>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </Card>
      </Col>
    )
  }
}

export default InverterCard