import React from 'react'
import history from '../../../history'
import { Row, Col } from 'antd'
import InverterCard from './inverter-card'

const InverterCardsWrapper = (props) => {
  return (
    <div className="cards">
      <Row>
        <Col xs={{ span: 20, offset: 2 }} xl={{ span: 19, offset: 2 }}>
          {props.inverterCards.map((card) =>
            <InverterCard card={card} selectInverter={() => history.push('/client/sites/' + props.siteId + '/' + card.inverterId)} inverterCards={props.inverterCards} />
          )}
        </Col>
      </Row>
    </div >
  )
}

export default InverterCardsWrapper