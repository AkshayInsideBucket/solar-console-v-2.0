import React from 'react'
import SiteTiles from './site-tiles'
import WrappedSiteChart from './site-chart-wrapper'
import InverterCardsWrapper from './inverter-cards-wrapper'
import SensorReadings from './sensor-readings'

class ClientSite extends React.Component {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    this.props.onMount()
  }
  render() {
    return (
      <div>
        <SiteTiles siteTiles={this.props.siteTiles} />
        <WrappedSiteChart energyChartData={this.props.energyChartData} energySet={this.props.energySet} />
        <InverterCardsWrapper inverterCards={this.props.inverterCards} siteId={this.props.match.params.siteId} />
        <SensorReadings sensorReadings={this.props.sensorReadings} />
      </div>
    )
  }
}

export default ClientSite