import React from 'react'
import { Router, Route } from 'react-router-dom';
import Topbar from './topbar/topbar'
import Sidenav from './sidenav/sidenav'
import ClientDashboardContainer from './client-dashboard/client-dashboard-container'
import ClientSiteContainer from './client-site/client-site-container'
import ClientInverterContainer from './client-inverter/client-inverter-container'
import ClientReportContainer from './client-report/client-report-container'

class ClientWrapper extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.onMount()
  }

  render() {
    return (
      <div className="client-wrapper">
        <Topbar logout={this.props.logout} />
        <div className="content-wrapper">
          <Sidenav url={this.props.location.pathname} logout={this.props.logout} accountName={this.props.accountName} />
          <div className="content">
            <Route
              exact
              path="/client"
              render={(routeProps) => (
                <ClientDashboardContainer {...routeProps} />
              )}
            />
            <Route
              exact
              path="/client/report"
              render={(routeProps) => (
                <ClientReportContainer {...routeProps} />
              )}
            />
            <Route
              exact
              path="/client/sites/:siteId"
              render={(routeProps) => (
                <ClientSiteContainer {...routeProps} />
              )}
            />
            <Route
              exact
              path="/client/sites/:siteId/:inverterId"
              render={(routeProps) => (
                <ClientInverterContainer {...routeProps} />
              )}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default ClientWrapper