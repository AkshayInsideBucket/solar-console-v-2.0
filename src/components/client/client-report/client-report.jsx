import React from 'react'
import { Row, Col, Table, Divider, Modal, DatePicker, message, Select } from 'antd'
import moment from 'moment';
import DailyReport from './daily-report/daily-report'
import SiteReport from './weekly-monthly/site-report/site-report';
import InverterReport from './weekly-monthly/inverter-report/inverter-report';
import YearlyReport from './yearly-report/yearly-report'

class ClientReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      modalTitle: '',
      dailyDate: '',
      siteStartWeek: '',
      siteEndWeek: '',
      inverterStartWeek: '',
      inverterEndWeek: '',
      siteMonthDate: '',
      inverterMonthDate: '',
      yearlyDate: '',
      tableDataToggle: false
    };
  }

  onPreview = (report) => {
    switch (report) {
      case 'Daily Report':
        if (this.state.dailyDate === '') {
          this.setState({
            visible: false,
          });
          message.warning('Please select a date', 0.5);
        } else {
          this.setState({
            visible: true,
            modalTitle: report,
          });
        }
        break;
      case 'Weekly Report (Site)':
        if (this.state.siteStartWeek === '' && this.state.siteEndWeek === '') {
          this.setState({
            visible: false,
          });
          message.warning('Please select a date', 0.5);
        } else {
          this.setState({
            visible: true,
            modalTitle: report,
          });
        }
        break;
      case 'Weekly Report (Inverter)':
        if (this.state.inverterStartWeek === '' && this.state.inverterEndWeek === '') {
          this.setState({
            visible: false,
          });
          message.warning('Please select a date', 0.5);
        } else {
          this.setState({
            visible: true,
            modalTitle: report,
          });
        }
        break;
      case 'Monthly Report (Site)':
        if (this.state.siteMonthDate === '') {
          this.setState({
            visible: false,
          });
          message.warning('Please select a month', 0.5);
        } else {
          this.setState({
            visible: true,
            modalTitle: report,
          });
        }
        break;
      case 'Monthly Report (Inverter)':
        if (this.state.inverterMonthDate === '') {
          this.setState({
            visible: false,
          });
          message.warning('Please select a month', 0.5);
        } else {
          this.setState({
            visible: true,
            modalTitle: report,
          });
        }
        break;
      case 'Yearly Report':
        if (this.state.yearlyDate === '' || this.state.yearlyDate === undefined) {
          this.setState({
            visible: false,
          });
          message.warning('Please select a year', 0.5);
        } else {
          this.setState({
            visible: true,
            modalTitle: report,
          });
        }
    }
  }

  onDownload = (report) => {
    console.log(report)
  }

  handleCancel = (e) => {
    this.setState({
      visible: false,
      tableDataToggle: false
    });
  }

  onTableDataToggle = () => {
    this.setState({
      tableDataToggle: true
    });
  }

  onDateChange = (date, dateString) => {
    this.setState({
      dailyDate: dateString
    });
  }

  onSiteStartWeekChange = (date, dateString) => {
    this.setState({
      siteStartWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY')
        ),
      siteEndWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY').add(7, 'days')
        )
    });
  }

  onInverterStartWeekChange = (date, dateString) => {
    this.setState({
      inverterStartWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY')
        ),
      inverterEndWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY').add(7, 'days')
        )
    });
  }

  onSiteEndWeekChange = (date, dateString) => {
    this.setState({
      siteStartWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY').subtract(7, 'days')
        ),
      siteEndWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY')
        )

    });
  }

  onInverterEndWeekChange = (date, dateString) => {
    this.setState({
      inverterStartWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY').subtract(7, 'days')
        ),
      inverterEndWeek:
        (
          (dateString === '')
            ?
            ''
            :
            moment(dateString, 'DD/MM/YYYY')
        )

    });
  }

  onSiteMonthChange = (date, dateString) => {
    this.setState({
      siteMonthDate: dateString
    });
  }

  onInverterMonthChange = (date, dateString) => {
    this.setState({
      inverterMonthDate: dateString
    });
  }

  onYearChange = (value) => {
    this.setState({
      yearlyDate: value
    });
  }

  previousYears = () => {
    var array = []
    var year = new Date().getFullYear()
    for (let i = 0; i < 5; i++) {
      array.push(year - i)
    }
    return array
  }

  render() {
    const dateFormat = 'DD/MM/YYYY';
    const { MonthPicker } = DatePicker;
    const Option = Select.Option
    const columns = [
      {
        title: 'Report',
        dataIndex: 'report',
        key: 'report'
      },
      {
        title: 'Select Date',
        dataIndex: 'select',
        key: 'select',
        render: (text, record) => (
          (record.report === 'Daily Report')
            ?
            <DatePicker format={dateFormat} onChange={this.onDateChange} />
            :
            (record.report === 'Weekly Report (Site)')
              ?
              < div >
                <DatePicker
                  placeholder="Start date"
                  format={dateFormat}
                  value={this.state.siteStartWeek}
                  onChange={this.onSiteStartWeekChange}
                />
                <br /> <br />
                <DatePicker
                  placeholder="End date"
                  format={dateFormat}
                  value={this.state.siteEndWeek}
                  onChange={this.onSiteEndWeekChange}
                />
              </div >
              :
              (record.report === 'Weekly Report (Inverter)')
                ?
                < div >
                  <DatePicker
                    placeholder="Start date"
                    format={dateFormat}
                    value={this.state.inverterStartWeek}
                    onChange={this.onInverterStartWeekChange}
                  />
                  <br /> <br />
                  <DatePicker
                    placeholder="End date"
                    format={dateFormat}
                    value={this.state.inverterEndWeek}
                    onChange={this.onInverterEndWeekChange}
                  />
                </div >
                :
                (record.report === 'Monthly Report (Site)')
                  ?
                  <MonthPicker placeholder="Select month" format={'MM/YYYY'} onChange={this.onSiteMonthChange} />
                  :
                  (record.report === 'Monthly Report (Inverter)')
                    ?
                    <MonthPicker placeholder="Select month" format={'MM/YYYY'} onChange={this.onInverterMonthChange} />
                    :
                    (record.report === 'Yearly Report')
                      ?
                      <Select allowClear={true} placeholder="Select a year" onChange={this.onYearChange}>
                        {
                          this.previousYears().map((year) =>
                            <Option value={year}>{year}</Option>
                          )
                        }

                      </Select>
                      : null
        )
      },
      {
        title: 'Actions',
        dataIndex: 'actions',
        key: 'actions',
        render: (text, record) => (
          <span>
            <a onClick={() => this.onPreview(record.report)}>Preview</a>
          </span>
        )
      }
    ]

    const dataSource = [
      {
        report: 'Daily Report'
      },
      {
        report: 'Weekly Report (Site)'
      },
      {
        report: 'Weekly Report (Inverter)'
      },
      {
        report: 'Monthly Report (Site)'
      },
      {
        report: 'Monthly Report (Inverter)'
      },
      {
        report: 'Yearly Report'
      },
    ]

    return (
      <div className="client-report">
        <Row>
          <Col xs={{ span: 24, offset: 0 }} xs={{ span: 22, offset: 1 }} >
            <Table pagination={false} columns={columns} dataSource={dataSource} />
          </Col>
        </Row>
        <Modal
          title={this.state.modalTitle}
          destroyOnClose={true}
          width={1000}
          visible={this.state.visible}
          onOk={this.handleOk}
          footer={null}
          onCancel={this.handleCancel}
        >
          {
            (this.state.modalTitle === 'Daily Report')
              ?
              <DailyReport onTableDataToggle={this.onTableDataToggle} tableDataToggle={this.state.tableDataToggle} spinTrigger={this.props.spinTrigger} dailyReportSelect={this.props.dailyReportSelect} sitesAndInverters={this.props.sitesAndInverters} dailyReport={this.props.dailyReport} date={this.state.dailyDate} />
              :
              (this.state.modalTitle === 'Weekly Report (Site)' || this.state.modalTitle === 'Monthly Report (Site)')
                ?
                <SiteReport onTableDataToggle={this.onTableDataToggle} tableDataToggle={this.state.tableDataToggle} spinTrigger={this.props.spinTrigger} weeklySiteReportSelect={this.props.weeklySiteReportSelect} monthlySiteReportSelect={this.props.monthlySiteReportSelect} sitesAndInverters={this.props.sitesAndInverters} invertersId={this.props.invertersId} siteReport={this.props.siteReport} reportType={this.state.modalTitle} siteMonthDate={this.state.siteMonthDate} siteEndDate={this.state.siteEndWeek} siteStartDate={this.state.siteStartWeek} />
                :
                (this.state.modalTitle === 'Weekly Report (Inverter)' || this.state.modalTitle === 'Monthly Report (Inverter)')
                  ?
                  <InverterReport onTableDataToggle={this.onTableDataToggle} tableDataToggle={this.state.tableDataToggle} spinTrigger={this.props.spinTrigger} weeklyInverterReportSelect={this.props.weeklyInverterReportSelect} monthlyInverterReportSelect={this.props.monthlyInverterReportSelect} sitesAndInverters={this.props.sitesAndInverters} inverterReport={this.props.inverterReport} reportType={this.state.modalTitle} inverterMonthDate={this.state.inverterMonthDate} inverterEndDate={this.state.inverterEndWeek} inverterStartDate={this.state.inverterStartWeek} />
                  :
                  (this.state.modalTitle === 'Yearly Report')
                    ?
                    <YearlyReport onTableDataToggle={this.onTableDataToggle} tableDataToggle={this.state.tableDataToggle} spinTrigger={this.props.spinTrigger} yearlyReportSelect={this.props.yearlyReportSelect} sitesAndInverters={this.props.sitesAndInverters} invertersId={this.props.invertersId} yearlyReport={this.props.yearlyReport} date={this.state.yearlyDate} />
                    : null
          }
        </Modal>
      </div>
    )
  }
}

export default ClientReport