import React from 'react'
import { Row, Col, Select } from 'antd'
import ReportOverview from './report-overview'
import ReportSummary from './report-summary'
import { CSVLink } from 'react-csv';

class YearlyReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteInfo: {
        siteId: '',
        siteName: ''
      }
    };
    this.handleSiteChange = this.handleSiteChange.bind(this);
  }

  handleSiteChange = (value) => {
    this.setState({
      siteInfo: this.props.sitesAndInverters.find((e) => e.siteId === value)
    });
    this.props.yearlyReportSelect(this.props.date, value)
    this.props.onTableDataToggle();
  }

  render() {
    const Option = Select.Option;

    const timeConversion = (millis) => {
      var date = new Date(millis);
      var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ];
      return monthNames[date.getMonth()];
    }

    const calculateEnergy = (inverters) => {
      let totalEnergy = 0;
      inverters.forEach((inverter) =>
        totalEnergy = totalEnergy + inverter.energy
      )
      return totalEnergy
    }

    const csvData = [
      ...[
        ['Yearly Report'],
        [],
        ['Report Overview'],
        [],
        ['Site Name :', this.state.siteInfo.siteName],
        ['System Size :', (this.props.yearlyReport.systemSize / 1000) + ' kW'],
        ["Total Energy :", (this.props.yearlyReport.totalEnergy) + ' kWh'],
        ["Total Income :", 'Rs ' + (this.props.yearlyReport.totalIncome)],
        ['Year :', this.props.date],
        [],
        ['Report Summary'],
        [],
        [
          ...[
            'Month',
            'Total (kWh)'
          ],
          ...this.props.invertersId.map((id) => {
            return [
              'Inverter ' + id + '(kWh)'
            ]
          }
          )
        ]
      ],
      ...this.props.yearlyReport.tableData.map((data) => {
        return [
          ...[
            timeConversion(data.month),
            calculateEnergy(data.inverters)
          ],
          ...data.inverters.map((inverter) =>
            inverter.energy
          )
        ]
      }
      )
    ];

    return (
      <div className="report-modal">
        <Row>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 12, offset: 0 }}>
            <Select defaultValue="Select a Site" style={{ width: '100%', marginBottom: '20px' }} onChange={this.handleSiteChange}>
              {this.props.sitesAndInverters.map((site) =>
                <Option value={site.siteId}>{site.siteName}</Option>
              )}
            </Select>
          </Col>
        </Row>
        <div style={{ marginBottom: '8px' }}>
          <p className="overview-title">Report Overview :-</p>
          <ReportOverview spinTrigger={this.props.spinTrigger} siteId={this.state.siteInfo.siteId} siteName={this.state.siteInfo.siteName} yearlyReport={this.props.yearlyReport} date={this.props.date} />
        </div>
        <div style={{ marginBottom: '12px' }}>
          <p className="summary-title">Report Summary :-</p>
          <CSVLink
            data={csvData}
            filename={"yearly-report.csv"}
            className={(this.state.siteInfo.siteId === '') ? "ant-btn ant-btn-primary disabled csv-link" : "ant-btn ant-btn-primary csv-link"}
            target="_blank">
            <i class="anticon anticon-download"></i>
            <span>Download</span>
          </CSVLink>
        </div>
        <ReportSummary spinTrigger={this.props.spinTrigger} tableDataToggle={this.props.tableDataToggle} invertersId={this.props.invertersId} siteId={this.state.siteInfo.siteId} yearlyReport={this.props.yearlyReport} />
      </div >
    )
  }
}

export default YearlyReport