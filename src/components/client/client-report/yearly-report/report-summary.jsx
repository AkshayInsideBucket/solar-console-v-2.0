import React from 'react'
import { Row, Col, Table } from 'antd'

class ReportSummary extends React.Component {

  render() {
    const columns = [...[
      {
        title: 'Month',
        dataIndex: 'month',
        key: 'month'
      },
      {
        title: 'Total (kWh)',
        dataIndex: 'total',
        key: 'total'
      },
    ],
    ...this.props.invertersId.map((id) => {
      return {
        title: 'Inverter ' + id + ' (kWh)',
        dataIndex: id,
        key: id
      }
    }
    )
    ]

    const timeConversion = (millis) => {
      var date = new Date(millis);
      var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ];
      return monthNames[date.getMonth()];
    }

    const dataSource = (
      this.props.yearlyReport.tableData.map((data) => {
        let totalEnergy = 0;
        data.inverters.forEach(inverter => totalEnergy = totalEnergy + inverter.energy)
        let obj = {
          month: timeConversion(data.month),
          total: totalEnergy
        }
        data.inverters.forEach(inverter => obj[inverter.inverterId] = inverter.energy)
        return obj
      }
      )
    )

    return (
      <div className="report-summary" >
        < Table
          pagination={{ defaultPageSize: 6 }}
          columns={columns}
          dataSource={(this.props.tableDataToggle === true && this.props.spinTrigger === false) ? dataSource : []} />
      </div>
    )
  }
}

export default ReportSummary