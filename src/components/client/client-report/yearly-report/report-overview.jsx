import React from 'react'
import { Row, Col, Spin } from 'antd'
import moment from 'moment';

class ReportOverview extends React.Component {
  render() {
    return (
      <Row className="overview-div">
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Site Name :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                this.props.siteName
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">System Size :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                (this.props.yearlyReport.systemSize / 1000) + ' kW'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Total Energy :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                (this.props.yearlyReport.totalEnergy) + ' kWh'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Total Income :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                'Rs ' + (this.props.yearlyReport.totalIncome)
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Year :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">{this.props.date}
          </p>
        </Col>
        <Spin spinning={this.props.spinTrigger} />
      </Row>
    )
  }
}

export default ReportOverview