import React from 'react'
import { Row, Col, Spin } from 'antd'
import moment from 'moment';

class ReportOverview extends React.Component {
  render() {
    return (
      <Row className="overview-div">
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Site Name :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                this.props.siteName
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">System Size :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                (this.props.siteReport.systemSize / 1000) + ' kW'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Total Energy :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                (this.props.siteReport.totalEnergy) + ' kWh'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Total Income :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteId === '')
                ?
                '-'
                :
                'Rs ' + (this.props.siteReport.totalIncome)
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Date :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.reportType === 'Weekly Report (Site)')
                ?
                moment(this.props.startDate).format("DD/MM/YYYY") + '-' + moment(this.props.endDate).format("DD/MM/YYYY")
                :
                (this.props.reportType === 'Monthly Report (Site)')
                  ?
                  this.props.siteMonthDate
                  : null
            }
          </p>
        </Col>
        <Spin spinning={this.props.spinTrigger} />
      </Row>
    )
  }
}

export default ReportOverview