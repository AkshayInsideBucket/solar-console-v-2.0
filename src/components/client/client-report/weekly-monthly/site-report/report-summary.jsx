import React from 'react'
import { Row, Col, Table } from 'antd'

class ReportSummary extends React.Component {

  render() {
    const columns = [...[
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date'
      },
      {
        title: 'Total (kWh)',
        dataIndex: 'total',
        key: 'total'
      },
    ],
    ...this.props.invertersId.map((id) => {
      return {
        title: 'Inverter ' + id + ' (kWh)',
        dataIndex: id,
        key: id
      }
    }
    )
    ]

    const timeConversion = (millis) => {
      var date = new Date(millis);
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      if (mm < 10) {
        var month = '0' + mm
      }
      var yy = date.getFullYear()
      return dd + '/' + month + '/' + yy
    }

    const dataSource = (
      this.props.siteReport.tableData.map((data) => {
        let totalEnergy = 0;
        data.inverters.forEach(inverter => totalEnergy = totalEnergy + inverter.energy)
        let obj = { 
          date: timeConversion(data.date),
          total: totalEnergy
        }
        data.inverters.forEach(inverter => obj[inverter.inverterId] = inverter.energy)
        return obj
      }
      )
    )

    return (
      <div className="report-summary" >
        < Table pagination={{ defaultPageSize: 6 }} columns={columns} dataSource={(this.props.tableDataToggle === true && this.props.spinTrigger === false) ? dataSource : []} />
      </div>
    )
  }
}

export default ReportSummary