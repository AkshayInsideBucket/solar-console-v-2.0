import React from 'react'
import { Row, Col, Select } from 'antd'
import ReportOverview from './report-overview'
import ReportSummary from './report-summary'
import { CSVLink } from 'react-csv';
import moment from 'moment';

class SiteReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteInfo: {
        siteId: '',
        siteName: ''
      }
    };
    this.handleSiteChange = this.handleSiteChange.bind(this);
  }

  handleSiteChange = (value) => {
    this.setState({
      siteInfo: this.props.sitesAndInverters.find((e) => e.siteId === value)
    });
    if (this.props.reportType === 'Weekly Report (Site)') {
      this.props.weeklySiteReportSelect(
        moment(this.props.siteStartDate).format("DD/MM/YYYY"),
        moment(this.props.siteEndDate).format("DD/MM/YYYY"),
        value
      )
      this.props.onTableDataToggle();
    }
    if (this.props.reportType === 'Monthly Report (Site)') {
      this.props.monthlySiteReportSelect(this.props.siteMonthDate, value)
      this.props.onTableDataToggle();
    }
  }

  render() {
    const Option = Select.Option;

    const timeConversion = (millis) => {
      var date = new Date(millis);
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      if (mm < 10) {
        var month = '0' + mm
      }
      var yy = date.getFullYear()
      return dd + '/' + month + '/' + yy
    }

    const calculateEnergy = (inverters) => {
      let totalEnergy = 0;
      inverters.forEach((inverter) =>
        totalEnergy = totalEnergy + inverter.energy
      )
      return totalEnergy
    }

    const csvData = [
      ...[
        [
          (this.props.reportType === 'Weekly Report (Site)')
            ?
            'Weekly Report (Site)'
            :
            (this.props.reportType === 'Monthly Report (Site)')
              ?
              'Monthly Report (Site)'
              : null
        ],
        [],
        ['Report Overview'],
        [],
        ['Site Name :', this.state.siteInfo.siteName],
        ['System Size :', (this.props.siteReport.systemSize / 1000) + ' kW'],
        ["Total Energy :", (this.props.siteReport.totalEnergy) + ' kWh'],
        ["Total Income :", 'Rs ' + (this.props.siteReport.totalIncome)],
        ['Date :',
          (this.props.reportType === 'Weekly Report (Site)')
            ?
            moment(this.props.siteStartDate).format("DD/MM/YYYY") + '-' + moment(this.props.siteEndDate).format("DD/MM/YYYY")
            :
            (this.props.reportType === 'Monthly Report (Site)')
              ?
              this.props.siteMonthDate
              : null
        ],
        [],
        ['Report Summary'],
        [],
        [
          ...[
            'Date',
            'Total (kWh)'
          ],
          ...this.props.invertersId.map((id) => {
            return [
              'Inverter ' + id + '(kWh)'
            ]
          }
          )
        ]
      ],
      ...this.props.siteReport.tableData.map((data) => {
        return [
          ...[
            timeConversion(data.date),
            calculateEnergy(data.inverters)
          ],
          ...data.inverters.map((inverter) =>
            inverter.energy
          )
        ]
      }
      )
    ];

    return (
      <div className="report-modal">
        <Row>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 12, offset: 0 }}>
            <Select defaultValue="Select a Site" style={{ width: '100%', marginBottom: '20px' }} onChange={this.handleSiteChange}>
              {this.props.sitesAndInverters.map((site) =>
                <Option value={site.siteId}>{site.siteName}</Option>
              )}
            </Select>
          </Col>
        </Row>
        <div style={{ marginBottom: '8px' }}>
          <p className="overview-title">Report Overview :-</p>
          <ReportOverview reportType={this.props.reportType} siteMonthDate={this.props.siteMonthDate} siteReport={this.props.siteReport} endDate={this.props.siteEndDate} startDate={this.props.siteStartDate} spinTrigger={this.props.spinTrigger} siteId={this.state.siteInfo.siteId} siteName={this.state.siteInfo.siteName} />
        </div>
        <div style={{ marginBottom: '12px' }}>
          <p className="summary-title">Report Summary :-</p>
          <CSVLink
            data={csvData}
            filename={
              (this.props.reportType === 'Weekly Report (Site)')
                ?
                'weekly-report(site).csv'
                :
                (this.props.reportType === 'Monthly Report (Site)')
                  ?
                  'monthly-report(site).csv'
                  : null
            }
            className={(this.state.siteInfo.siteId === '') ? "ant-btn ant-btn-primary disabled csv-link" : "ant-btn ant-btn-primary csv-link"}
            target="_blank">
            <i class="anticon anticon-download"></i>
            <span>Download</span>
          </CSVLink>
        </div>
        <ReportSummary spinTrigger={this.props.spinTrigger} tableDataToggle={this.props.tableDataToggle} invertersId={this.props.invertersId} siteId={this.state.siteInfo.siteId} siteReport={this.props.siteReport} />
      </div >
    )
  }
}

export default SiteReport