import React from 'react'
import { Row, Col, Spin } from 'antd'
import moment from 'moment';

class ReportOverview extends React.Component {
  render() {
    return (
      <Row className="overview-div">
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Site Name :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteName === undefined)
                ?
                '-'
                :
                this.props.siteName
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Inverter ID :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                this.props.inverterId
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">System Size :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                (this.props.inverterReport.systemSize / 1000) + ' kW'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Total Energy :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                (this.props.inverterReport.totalEnergy) + ' kWh'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Total Income :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                'Rs ' + (this.props.inverterReport.totalIncome)
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Date :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.reportType === 'Weekly Report (Inverter)')
                ?
                moment(this.props.startDate).format("DD/MM/YYYY") + '-' + moment(this.props.endDate).format("DD/MM/YYYY")
                :
                (this.props.reportType === 'Monthly Report (Inverter)')
                  ?
                  this.props.inverterMonthDate
                  : null
            }
          </p>
        </Col>
        <Spin spinning={this.props.spinTrigger} />
      </Row>
    )
  }
}

export default ReportOverview