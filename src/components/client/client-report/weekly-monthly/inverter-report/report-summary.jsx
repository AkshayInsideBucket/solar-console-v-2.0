import React from 'react'
import { Row, Col, Table } from 'antd'

class ReportSummary extends React.Component {

  render() {
    const columns = [
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date'
      },
      {
        title: 'Energy (kWh)',
        dataIndex: 'energy',
        key: 'energy'
      },
      {
        title: 'Peak Power (kW)',
        dataIndex: 'peakPower',
        key: 'peakPower'
      }
    ]

    const timeConversion = (millis) => {
      var date = new Date(millis);
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      if (mm < 10) {
        var month = '0' + mm
      }
      var yy = date.getFullYear()
      return dd + '/' + month + '/' + yy
    }

    const dataSource = (
      this.props.inverterReport.tableData.map((data) => {
        return {
          date: timeConversion(data.date),
          energy: data.energy,
          peakPower: data.peakPower / 1000
        }
      }
      )
    )

    return (
      <div className="report-summary" >
        < Table pagination={{ defaultPageSize: 6 }} columns={columns} dataSource={(this.props.tableDataToggle === true && this.props.spinTrigger === false) ? dataSource : []} />
      </div>
    )
  }
}

export default ReportSummary