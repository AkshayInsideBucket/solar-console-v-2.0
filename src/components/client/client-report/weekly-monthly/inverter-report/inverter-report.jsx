import React from 'react'
import { Row, Col, Select } from 'antd'
import ReportOverview from './report-overview'
import ReportSummary from './report-summary'
import { CSVLink } from 'react-csv';
import moment from 'moment';

class InverterReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteInfo: '',
      inverterId: ''
    };
    this.handleSiteChange = this.handleSiteChange.bind(this);
    this.handleInverterChange = this.handleInverterChange.bind(this);
  }

  handleSiteChange = (value) => {
    this.setState({
      siteInfo: this.props.sitesAndInverters.find((e) => e.siteId === value)
    });
  }

  handleInverterChange = (value) => {
    this.setState({
      inverterId: value
    });
    if (this.props.reportType === 'Weekly Report (Inverter)') {
      this.props.weeklyInverterReportSelect(
        moment(this.props.inverterStartDate).format("DD/MM/YYYY"),
        moment(this.props.inverterEndDate).format("DD/MM/YYYY"),
        this.state.siteInfo.siteId,
        value
      );
      this.props.onTableDataToggle();
    }
    if (this.props.reportType === 'Monthly Report (Inverter)') {
      this.props.monthlyInverterReportSelect(
        this.props.inverterMonthDate,
        this.state.siteInfo.siteId,
        value
      );
      this.props.onTableDataToggle();
    }
  }

  render() {
    const Option = Select.Option;

    const timeConversion = (millis) => {
      var date = new Date(millis);
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      if (mm < 10) {
        var month = '0' + mm
      }
      var yy = date.getFullYear()
      return dd + '/' + month + '/' + yy
    }

    const csvData = [
      ...[
        [
          (this.props.reportType === 'Weekly Report (Inverter)')
            ?
            'Weekly Report (Inverter)'
            :
            (this.props.reportType === 'Monthly Report (Inverter)')
              ?
              'Monthly Report (Inverter)'
              : null
        ],
        [],
        ['Report Overview'],
        [],
        ['Site Name :', this.state.siteInfo.siteName],
        ['Inverter ID :', this.state.inverterId],
        ['System Size :', (this.props.inverterReport.systemSize / 1000) + ' kW'],
        ["Total Energy :", (this.props.inverterReport.totalEnergy) + ' kWh'],
        ["Total Income :", 'Rs ' + (this.props.inverterReport.totalIncome)],
        ['Date :',
          (this.props.reportType === 'Weekly Report (Inverter)')
            ?
            moment(this.props.inverterStartDate).format("DD/MM/YYYY") + '-' + moment(this.props.inverterEndDate).format("DD/MM/YYYY")
            :
            (this.props.reportType === 'Monthly Report (Inverter)')
              ?
              this.props.inverterMonthDate
              : null
        ],
        [],
        ['Report Summary'],
        [],
        [
          'Date',
          'Energy (kWh)',
          'Peak Power (kW)'
        ]
      ],
      ...this.props.inverterReport.tableData.map((data) => {
        return [
          timeConversion(data.date),
          data.energy,
          data.peakPower / 1000
        ]
      }
      )
    ];

    return (
      <div className="report-modal">
        <Row>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 11, offset: 0 }}>
            <Select defaultValue="Select a Site" style={{ width: '100%', marginBottom: '20px' }} onChange={this.handleSiteChange}>
              {this.props.sitesAndInverters.map((site) =>
                <Option value={site.siteId}>{site.siteName}</Option>
              )}
            </Select>
          </Col>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 11, offset: 1 }}>
            <Select defaultValue="Select a Inverter" style={{ width: '100%', marginBottom: '20px' }} onChange={this.handleInverterChange}>
              {
                (this.state.siteInfo === '')
                  ?
                  (
                    null
                  )
                  :
                  this.state.siteInfo.inverters.map((inverter) =>
                    <Option value={inverter.inverterId}>{inverter.inverterId}</Option>
                  )
              }
            </Select>
          </Col>
        </Row>
        <div style={{ marginBottom: '8px' }}>
          <p className="overview-title">Report Overview :-</p>
          <ReportOverview reportType={this.props.reportType} inverterMonthDate={this.props.inverterMonthDate} inverterReport={this.props.inverterReport} spinTrigger={this.props.spinTrigger} inverterId={this.state.inverterId} siteName={this.state.siteInfo.siteName} endDate={this.props.inverterEndDate} startDate={this.props.inverterStartDate} />
        </div>
        <div style={{ marginBottom: '12px' }}>
          <p className="summary-title">Report Summary :-</p>
          <CSVLink
            data={csvData}
            filename={
              (this.props.reportType === 'Weekly Report (Inverter)')
                ?
                'weekly-report(inverter).csv'
                :
                (this.props.reportType === 'Monthly Report (Inverter)')
                  ?
                  'monthly-report(inverter).csv'
                  : null
            }
            className={(this.state.inverterId === '') ? "ant-btn ant-btn-primary disabled csv-link" : "ant-btn ant-btn-primary csv-link"}
            target="_blank">
            <i class="anticon anticon-download"></i>
            <span>Download</span>
          </CSVLink>
        </div>

        <ReportSummary spinTrigger={this.props.spinTrigger} tableDataToggle={this.props.tableDataToggle} inverterId={this.state.inverterId} inverterReport={this.props.inverterReport} />
      </div >
    )
  }
}

export default InverterReport