import { connect } from 'react-redux'
import { call } from '../../../reducers/client-api'
import ClientReport from './client-report'
import groupBy from '../../../utils/groupBy';
import store from '../../../store';

function getSitesAndInverters(readings) {
  var invertersBySite = groupBy(readings, 'site_id')
  var result = []
  for (var key in invertersBySite) {
    if (invertersBySite.hasOwnProperty(key)) {
      result.push({ siteId: key, siteName: invertersBySite[key][0].name, inverters: invertersBySite[key].map(obj => { return { inverterId: obj.inverter_id } }) })
    }
  }
  return result
}

function getYearlyReportTable(readings) {
  var readingsByMonth = groupBy(readings, 'month')
  var result = []
  for (var key in readingsByMonth) {
    if (readingsByMonth.hasOwnProperty(key)) {
      result.push({ month: key, inverters: readingsByMonth[key].map(obj => { return { inverterId: obj.inverter_id, energy: obj.energy } }) })
    }
  }
  return result
}

function getSiteReportTable(readings) {
  var readingsByDate = groupBy(readings, 'day')
  var result = []
  for (var key in readingsByDate) {
    if (readingsByDate.hasOwnProperty(key)) {
      result.push({ date: key, inverters: readingsByDate[key].map(obj => { return { inverterId: obj.inverter_id, energy: obj.energy } }) })
    }
  }
  return result
}

const mapDispatchToProps = (dispatch) => {
  return {
    dailyReportSelect: (date, siteId, inverterId) => {
      const temp = date.split('/')
      dispatch(call('fetch-daily-report', { sessionId: store.getState().account.sessionId, siteId: siteId, inverterId: inverterId, date: temp[2] + '-' + temp[1] + '-' + temp[0] }))
      dispatch({ type: 'client.reports.requested', operation: 'set', value: true })
    },
    weeklySiteReportSelect: (startDate, endDate, siteId) => {
      const temp1 = startDate.split('/')
      const temp2 = endDate.split('/')
      dispatch(call('fetch-site-report', { sessionId: store.getState().account.sessionId, siteId: siteId, from: temp1[2] + '-' + temp1[1] + '-' + temp1[0], to: temp2[2] + '-' + temp2[1] + '-' + temp2[0] }))
      dispatch({ type: 'client.reports.requested', operation: 'set', value: true })
      dispatch({ type: 'client.reports.selectedSite', operation: 'set', value: siteId })
    },
    monthlySiteReportSelect: (month, siteId) => {
      const temp = month.split('/')
      const date = new Date(temp[0] + '/01/' + temp[1]);
      const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      const from = temp[1] + '-' + temp[0] + '-01'
      const to = temp[1] + '-' + temp[0] + '-' + lastDay.getDate()
      dispatch(call('fetch-site-report', { sessionId: store.getState().account.sessionId, siteId: siteId, from: from, to: to }))
      dispatch({ type: 'client.reports.requested', operation: 'set', value: true })
      dispatch({ type: 'client.reports.selectedSite', operation: 'set', value: siteId })
    },
    weeklyInverterReportSelect: (startDate, endDate, siteId, inverterId) => {
      const temp1 = startDate.split('/')
      const temp2 = endDate.split('/')
      dispatch(call('fetch-inverter-report', { sessionId: store.getState().account.sessionId, siteId: siteId, inverterId: inverterId, from: temp1[2] + '-' + temp1[1] + '-' + temp1[0], to: temp2[2] + '-' + temp2[1] + '-' + temp2[0] }))
      dispatch({ type: 'client.reports.requested', operation: 'set', value: true })
    },
    monthlyInverterReportSelect: (month, siteId, inverterId) => {
      const temp = month.split('/')
      const date = new Date(temp[0] + '/01/' + temp[1]);
      const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      const from = temp[1] + '-' + temp[0] + '-01'
      const to = temp[1] + '-' + temp[0] + '-' + lastDay.getDate()
      dispatch(call('fetch-inverter-report', { sessionId: store.getState().account.sessionId, siteId: siteId, inverterId: inverterId, from: from, to: to }))
      dispatch({ type: 'client.reports.requested', operation: 'set', value: true })
    },
    yearlyReportSelect: (year, siteId) => {
      dispatch(call('fetch-yearly-report', { sessionId: store.getState().account.sessionId, siteId: siteId, year: year }))
      dispatch({ type: 'client.reports.requested', operation: 'set', value: true })
      dispatch({ type: 'client.reports.selectedSite', operation: 'set', value: siteId })
    }
  }
}


const mapStateToProps = state => {
  return {
    dailyReport: {
      systemSize: state.client.reports.daily.overview.capacity,
      todayEnergy: state.client.reports.daily.overview.energy,
      todayIncome: state.client.reports.daily.overview.income,
      peakPower: state.client.reports.daily.overview.peak,
      mpptLevel: state.client.reports.daily.overview.mppt,
      tableData: state.client.reports.daily.table.map(obj => Object.assign({}, obj, { currentPower: obj.power }))
    },
    sitesAndInverters: getSitesAndInverters(state.client.readings),
    spinTrigger: state.client.reports.requested,
    siteReport: {
      systemSize: state.client.reports.site.overview.capacity,
      totalEnergy: state.client.reports.site.overview.energy,
      totalIncome: state.client.reports.site.overview.income,
      tableData: getSiteReportTable(state.client.reports.site.table)
    },
    invertersId: getSitesAndInverters(state.client.readings).find(obj => obj.siteId === state.client.reports.selectedSite) ? getSitesAndInverters(state.client.readings).find(obj => obj.siteId === state.client.reports.selectedSite).inverters.map(obj => obj.inverterId) : [],
    inverterReport: {
      systemSize: state.client.reports.inverter.overview.capacity,
      totalEnergy: state.client.reports.inverter.overview.energy,
      totalIncome: state.client.reports.inverter.overview.income,
      tableData: state.client.reports.inverter.table.map(obj => Object.assign({}, obj, { peakPower: obj.peak })),
    },
    yearlyReport: {
      systemSize: state.client.reports.yearly.overview.capacity,
      totalEnergy: state.client.reports.yearly.overview.energy,
      totalIncome: state.client.reports.yearly.overview.income,
      tableData: getYearlyReportTable(state.client.reports.yearly.table)
    }
  }
}

const ClientReportContainer = connect(mapStateToProps, mapDispatchToProps)(ClientReport)

export default ClientReportContainer
