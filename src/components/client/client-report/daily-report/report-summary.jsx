import React from 'react'
import { Row, Col, Table } from 'antd'

class ReportSummary extends React.Component {

  render() {
    const columns_1 = [
      {
        title: 'Time',
        dataIndex: 'time',
        key: 'time'
      },
      {
        title: 'Current Power',
        dataIndex: 'currentPower',
        key: 'currentPower'
      },
      {
        title: 'P1V (V)',
        dataIndex: 'p1v',
        key: 'p1v'
      },
      {
        title: 'P1P (kW)',
        dataIndex: 'p1p',
        key: 'p1p'
      },
      {
        title: 'P1I (A)',
        dataIndex: 'p1i',
        key: 'p1i'
      }
    ]

    const columns_2 = [
      {
        title: 'Time',
        dataIndex: 'time',
        key: 'time'
      },
      {
        title: 'Current Power',
        dataIndex: 'currentPower',
        key: 'currentPower'
      },
      {
        title: 'P1V (V)',
        dataIndex: 'p1v',
        key: 'p1v'
      },
      {
        title: 'P1P (kW)',
        dataIndex: 'p1p',
        key: 'p1p'
      },
      {
        title: 'P1I (A)',
        dataIndex: 'p1i',
        key: 'p1i'
      },
      {
        title: 'P2V (V)',
        dataIndex: 'p2v',
        key: 'p2v'
      },
      {
        title: 'P2P (kW)',
        dataIndex: 'p2p',
        key: 'p2p'
      },
      {
        title: 'P2I (A)',
        dataIndex: 'p2i',
        key: 'p2i'
      }
    ]

    const columns_3 = [
      {
        title: 'Time',
        dataIndex: 'time',
        key: 'time'
      },
      {
        title: 'Current Power (kW)',
        dataIndex: 'currentPower',
        key: 'currentPower'
      },
      {
        title: 'P1V (V)',
        dataIndex: 'p1v',
        key: 'p1v'
      },
      {
        title: 'P1P (kW)',
        dataIndex: 'p1p',
        key: 'p1p'
      },
      {
        title: 'P1I (A)',
        dataIndex: 'p1i',
        key: 'p1i'
      },
      {
        title: 'P2V (V)',
        dataIndex: 'p2v',
        key: 'p2v'
      },
      {
        title: 'P2P (kW)',
        dataIndex: 'p2p',
        key: 'p2p'
      },
      {
        title: 'P2I (A)',
        dataIndex: 'p2i',
        key: 'p2i'
      },
      {
        title: 'P3V (V)',
        dataIndex: 'p3v',
        key: 'p3v'
      },
      {
        title: 'P3P (kW)',
        dataIndex: 'p3p',
        key: 'p3p'
      },
      {
        title: 'P3I (A)',
        dataIndex: 'p3i',
        key: 'p3i'
      }
    ]

    const timeConversion = (millis) => {
      var date = new Date(millis);
      return date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    }

    const dataSource = (
      this.props.dailyReport.tableData.map((data) => {
        return {
          time: timeConversion(data.time),
          currentPower: data.currentPower / 1000,
          p1v: data.p1v / 10,
          p1p: data.p1p / 1000,
          p1i: data.p1i / 100,
          p2v: data.p2v / 10,
          p2p: data.p2p / 1000,
          p2i: data.p2i / 100,
          p3v: data.p3v / 10,
          p3p: data.p3p / 1000,
          p3i: data.p3i / 100
        }
      }
      )
    )

    return (
      <div className="report-summary">
        {
          (this.props.dailyReport.mpptLevel === 1)
            ?
            (
              < Table pagination={{ defaultPageSize: 6 }} columns={columns_1} dataSource={(this.props.tableDataToggle === true && this.props.spinTrigger === false) ? dataSource : []} />
            )
            :
            (this.props.dailyReport.mpptLevel === 2)
              ?
              (
                < Table pagination={{ defaultPageSize: 6 }} columns={columns_2} dataSource={(this.props.tableDataToggle === true && this.props.spinTrigger === false) ? dataSource : []} />
              )
              :
              (this.props.dailyReport.mpptLevel === 3)
                ?
                (
                  < Table pagination={{ defaultPageSize: 6 }} columns={columns_3} dataSource={(this.props.tableDataToggle === true && this.props.spinTrigger === false) ? dataSource : []} />
                )
                : null
        }
      </div>
    )
  }
}

export default ReportSummary