import React from 'react'
import { Row, Col, Spin } from 'antd'

class ReportOverview extends React.Component {
  render() {
    return (
      <Row className="overview-div">
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Site Name :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.siteName === undefined)
                ?
                '-'
                :
                this.props.siteName
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Inverter ID :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                this.props.inverterId
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">System Size :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                (this.props.dailyReport.systemSize / 1000) + ' kW'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Today's Energy :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                (this.props.dailyReport.todayEnergy) + ' kWh'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Today's Income :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                'Rs ' + (this.props.dailyReport.todayIncome)
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Peak Power :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">
            {
              (this.props.inverterId === '')
                ?
                '-'
                :
                (this.props.dailyReport.peakPower / 1000) + ' kW'
            }
          </p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-type">Date :</p>
        </Col>
        <Col xs={{ span: 12, offset: 0 }}>
          <p className="overview-text">{this.props.date}</p>
        </Col>
        <Spin spinning={this.props.spinTrigger} />
      </Row>
    )
  }
}

export default ReportOverview