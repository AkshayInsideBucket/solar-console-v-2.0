import React from 'react'
import { Row, Col, Select, Button } from 'antd'
import ReportOverview from './report-overview'
import ReportSummary from './report-summary'
import { CSVLink } from 'react-csv';

class DailyReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteInfo: '',
      inverterId: ''
    };
    this.handleSiteChange = this.handleSiteChange.bind(this);
    this.handleInverterChange = this.handleInverterChange.bind(this);
  }

  handleSiteChange = (value) => {
    this.setState({
      siteInfo: this.props.sitesAndInverters.find((e) => e.siteId === value)
    });
  }

  handleInverterChange = (value) => {
    this.setState({
      inverterId: value
    });
    this.props.dailyReportSelect(this.props.date, this.state.siteInfo.siteId, value);
    this.props.onTableDataToggle();
  }

  render() {
    const Option = Select.Option;

    const timeConversion = (millis) => {
      var date = new Date(millis);
      return date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    }

    const csvData = [
      ...[
        ['Daily Report'],
        [],
        ['Report Overview'],
        [],
        ['Site Name :', this.state.siteInfo.siteName],
        ['Inverter ID :', this.state.inverterId],
        ['System Size :', (this.props.dailyReport.systemSize / 1000) + ' kW'],
        ["Today's Energy :", (this.props.dailyReport.todayEnergy) + ' kWh'],
        ["Today's Income :", 'Rs ' + (this.props.dailyReport.todayIncome)],
        ['Peak Power :', (this.props.dailyReport.peakPower / 1000) + ' kW'],
        ['Date :', this.props.date],
        [],
        ['Report Summary'],
        [],
        [
          ...[
            'Time',
            'Current Power (kW)',
            'P1V (V)',
            'P1P (kW)',
            'P1I (A)'
          ],
          ...
          (this.props.dailyReport.mpptLevel > 1)
            ?
            [
              'P2V (V)',
              'P2P (kW)',
              'P2I (A)'
            ]
            : [],
          ...
          (this.props.dailyReport.mpptLevel > 2)
            ?
            [
              'P3V (V)',
              'P3P (kW)',
              'P3I (A)'
            ]
            : []
        ]
      ],
      ...this.props.dailyReport.tableData.map((data) => {
        return [
          timeConversion(data.time),
          data.currentPower / 1000,
          data.p1v / 10,
          data.p1p / 1000,
          data.p1i / 100,
          data.p2v / 10,
          data.p2p / 1000,
          data.p2i / 100,
          data.p3v / 10,
          data.p3p / 1000,
          data.p3i / 100
        ]
      }
      )
    ];

    return (
      <div className="report-modal">
        <Row>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 11, offset: 0 }}>
            <Select defaultValue="Select a Site" style={{ width: '100%', marginBottom: '20px' }} onChange={this.handleSiteChange}>
              {this.props.sitesAndInverters.map((site) =>
                <Option value={site.siteId}>{site.siteName}</Option>
              )}
            </Select>
          </Col>
          <Col xs={{ span: 24, offset: 0 }} md={{ span: 11, offset: 1 }}>
            <Select defaultValue="Select a Inverter" style={{ width: '100%', marginBottom: '20px' }} onChange={this.handleInverterChange}>
              {
                (this.state.siteInfo === '')
                  ?
                  (
                    null
                  )
                  :
                  this.state.siteInfo.inverters.map((inverter) =>
                    <Option value={inverter.inverterId}>{inverter.inverterId}</Option>
                  )
              }
            </Select>
          </Col>
        </Row>
        <div style={{ marginBottom: '8px' }}>
          <p className="overview-title">Report Overview :-</p>
          <ReportOverview spinTrigger={this.props.spinTrigger} dailyReport={this.props.dailyReport} inverterId={this.state.inverterId} siteName={this.state.siteInfo.siteName} date={this.props.date} />
        </div>
        <div style={{ marginBottom: '12px' }}>
          <p className="summary-title">Report Summary :-</p>
          <CSVLink
            data={csvData}
            filename={"daily-report.csv"}
            className={(this.state.inverterId === '') ? "ant-btn ant-btn-primary disabled csv-link" : "ant-btn ant-btn-primary csv-link"}
            target="_blank">
            <i className="anticon anticon-download"></i>
            <span>Download</span>
          </CSVLink>
        </div>
        <ReportSummary spinTrigger={this.props.spinTrigger} tableDataToggle={this.props.tableDataToggle} inverterId={this.state.inverterId} dailyReport={this.props.dailyReport} />
      </div >
    )
  }
}

export default DailyReport