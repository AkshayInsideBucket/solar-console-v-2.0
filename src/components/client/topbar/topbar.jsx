import React from 'react'
import { Button } from 'antd';
import history from '../../../history'

const Topbar = (props) => {
  const ButtonGroup = Button.Group;

  return (
    <div className="topbar">
      <p onClick={() => history.push('/client')} > Client | Dashboard</p>
      <ButtonGroup>
        <Button onClick={() => history.push('/client/report')} type="primary" icon="file-text">Report</Button>
        <Button onClick={props.logout} id="logout-btn" type="primary" icon="logout">Logout</Button>
      </ButtonGroup>
    </div>
  )
}

export default Topbar