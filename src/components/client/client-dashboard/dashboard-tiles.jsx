import React from 'react'
import { Row, Col, Card } from 'antd'

const DashboardTiles = (props) => {
  return (
    <div className="tiles">
      <Row>
        <Col xs={{ span: 24, offset: 0 }} md={{ span: 20, offset: 2 }}>
          <div className="flex-container">
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons business-icon dashboard">business</i>
                    <div className="tiles-content">
                      <p className="value">{props.dashboardTiles.totalSites}</p>
                      <p className="description">Total Sites</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons flash_on-icon dashboard">flash_on</i>
                    <div className="tiles-content">
                      <p className="value">{(props.dashboardTiles.totalEnergy)+' kWh'}</p>
                      <p className="description">Total Energy</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons show_chart-icon dashboard">show_chart</i>
                    <div className="tiles-content">
                      <p className="value">{(props.dashboardTiles.totalHealth)+'%'}</p>
                      <p className="description">Total Health</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} xl={{ span: 6, offset: 0 }}>
              <div className="tiles-flexbox">
                <Card bordered={false}>
                  <div className="tiles-card">
                    <i className="material-icons account_balance_wallet-icon dashboard">account_balance_wallet</i>
                    <div className="tiles-content">
                      <p className="value">{'Rs '+(props.dashboardTiles.totalIncome)}</p>
                      <p className="description">Total Income</p>
                    </div>
                  </div>
                </Card>
              </div>
            </Col>
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default DashboardTiles