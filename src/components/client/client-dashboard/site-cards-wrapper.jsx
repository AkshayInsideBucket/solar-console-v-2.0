import React from 'react'
import { Row, Col } from 'antd'
import SiteCard from './site-card'

const SiteCardsWrapper = (props) => {
  return (
    <div className="cards">
      <Row>
        <Col xs={{ span: 20, offset: 2 }}>
          {props.siteCards.map((card) =>
            <SiteCard card={card} selectSite={props.selectSite} siteCards={props.siteCards}/>
          )}
        </Col>
      </Row>
    </div >
  )
}

export default SiteCardsWrapper