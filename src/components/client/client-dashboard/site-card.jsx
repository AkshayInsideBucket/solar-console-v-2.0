import React from 'react'
import { Row, Col, Card, Progress } from 'antd'
import history from '../../../history'

class SiteCard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timedOut: false
    }
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  tick() {
    const currentTime = new Date(new Date().toString())
    const lastTimeStamp = new Date(this.props.card.time)
    if ((currentTime - lastTimeStamp) > 1800000) this.setState({ timedOut: true })
    else this.setState({ timedOut: false })
  }

  render() {
    return (
      <Col className="cards-column" xs={{ span: 24, offset: 0 }} md={{ span: 12, offset: 0 }} xl={{ span: 8, offset: 0 }}>
        <Card bordered={false}>
          <div onClick={() => history.push('/client/sites/' + this.props.card.siteId)}>
            <p className="card-title">{this.props.card.siteName}</p>
            <div className="progress-bar">
              <Progress type="circle" percent={this.props.card.health} format={percent => `${this.props.card.health}%`} />
              <p className="progress-text">Health</p>
            </div>
            <div className="card-footer">
              <Row>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile corner-tile">
                    <i className="material-icons battery_charging_full-icon dashboard">battery_charging_full</i>
                    <div className="flex-text">
                      <p className="value">{this.props.card.inverters}</p>
                      <p className="description">Inverters</p>
                    </div>
                  </div>
                </Col>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile">
                    <i className="material-icons flash_on-icon dashboard">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{(this.props.card.monthlyEnergy) + ' kWh'}</p>
                      <p className="description">Monthly Energy</p>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile corner-tile">
                    <i className="material-icons account_balance_wallet-icon dashboard">account_balance_wallet</i>
                    <div className="flex-text">
                      <p className="value">{'Rs ' + (this.props.card.income)}{}</p>
                      <p className="description">Income</p>
                    </div>
                  </div>
                </Col>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile">
                    <i className="material-icons flash_on-icon dashboard">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{(this.props.card.annualEnergy) + ' kWh'}</p>
                      <p className="description">Annual Energy</p>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile corner-tile">
                    <i className="material-icons power-icon dashboard">power</i>
                    <div className="flex-text">
                      <p className="value">{(this.state.timedOut) ? '--' : ((this.props.card.currentPower / 1000) + ' kW')}</p>
                      <p className="description">Current Power</p>
                    </div>
                  </div>
                </Col>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile">
                    <i className="material-icons flash_on-icon dashboard">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{(this.state.timedOut) ? '--' : ((this.props.card.todayEnergy) + ' kWh')}</p>
                      <p className="description">Totay's Energy</p>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs={{ span: 12 }} xl={{ span: 12 }}>
                  <div className="flex-tile corner-tile">
                    <i className="material-icons flash_on-icon dashboard">flash_on</i>
                    <div className="flex-text">
                      <p className="value">{(this.props.card.totalEnergy) + ' kWh'}</p>
                      <p className="description">Total Energy</p>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </Card>
      </Col>
    )
  }
}

export default SiteCard