import { connect } from 'react-redux'
import store from '../../../store'
import history from '../../../history'
import { call } from '../../../reducers/client-api'
import ClientDashboard from './client-dashboard'
import calculateHealth from "../../../utils/health";
import groupBy from '../../../utils/groupBy';

const getSites = (readings, accType) => {
  if (accType === 'user') {
    let readingsBySite = groupBy(readings, 'site_id')
    for (var key in readingsBySite) {
      if (readingsBySite.hasOwnProperty(key)) {
        let reducedValue = readingsBySite[key].reduce((prev, curr) => {
          return Object.assign({}, prev, { todayEnergy: prev.todayEnergy + curr.etd, monthlyEnergy: prev.monthlyEnergy + curr.myd, annualEnergy: prev.annualEnergy + curr.ayd, totalEnergy: prev.totalEnergy + curr.tyd, currentPower: prev.currentPower + curr.apw, rate: curr.rate, time: new Date(curr.time).getTime() >= new Date(prev.time).getTime() ? curr.time : prev.time, siteId: curr.site_id, siteName: curr.name })
        }, { todayEnergy: 0, monthlyEnergy: 0, annualEnergy: 0, totalEnergy: 0, currentPower: 0, rate: 0, time: 0, siteId: '', siteName: '' })
        readingsBySite[key] = Object.assign({}, { health: calculateHealth(readingsBySite[key]), inverters: readingsBySite[key].length, income: reducedValue.todayEnergy * reducedValue.rate }, reducedValue)
      }
    }
    let sites = []
    for (var key in readingsBySite) {
      if (readingsBySite.hasOwnProperty(key)) {
        sites.push(readingsBySite[key])
      }
    }
    return sites
  } else if (accType === 'master') {
    let readingsByAccount = groupBy(readings, 'account')

    for (var key in readingsByAccount) {
      if (readingsByAccount.hasOwnProperty(key)) {

        let readingsBySite = groupBy(readingsByAccount[key], 'site_id')
        for (var key1 in readingsBySite) {
          if (readingsBySite.hasOwnProperty(key1)) {
            let reducedValue = readingsBySite[key1].reduce((prev, curr) => {
              return Object.assign({}, prev, { todayEnergy: prev.todayEnergy + curr.etd, monthlyEnergy: prev.monthlyEnergy + curr.myd, annualEnergy: prev.annualEnergy + curr.ayd, totalEnergy: prev.totalEnergy + curr.tyd, currentPower: prev.currentPower + curr.apw, rate: curr.rate, time: new Date(curr.time).getTime() >= new Date(prev.time).getTime() ? curr.time : prev.time, siteId: curr.site_id, account: curr.account, siteName: curr.name + ' (' + curr.account + ')', })
            }, { todayEnergy: 0, monthlyEnergy: 0, annualEnergy: 0, totalEnergy: 0, currentPower: 0, rate: 0, time: 0, siteId: '', account: '', siteName: '' })
            readingsBySite[key1] = Object.assign({}, { health: calculateHealth(readingsBySite[key1]), inverters: readingsBySite[key1].length, income: reducedValue.todayEnergy * reducedValue.rate }, reducedValue)
          }
        }
        let sites = []
        for (var key2 in readingsBySite) {
          if (readingsBySite.hasOwnProperty(key2)) sites.push(readingsBySite[key2])
        }

        readingsByAccount[key] = sites


      }
    }
    let finalSites = []

    for (var key in readingsByAccount) {
      if (readingsByAccount.hasOwnProperty(key)) {
        readingsByAccount[key].forEach((item) => {
          finalSites.push(item)
        })
      }
    }
    return finalSites
  } else return []
}

const getGlobalMetrics = (readings, accType) => {
  const readingsBySite = getSites(readings, accType)
  const globalResults = readingsBySite.reduce((prev, curr) => { return { totalEnergy: prev.totalEnergy + curr.totalEnergy, income: prev.income + curr.income } }, { income: 0, totalEnergy: 0 })
  return {
    totalSites: readingsBySite.length,
    totalEnergy: globalResults.totalEnergy,
    totalHealth: calculateHealth(readings),
    totalIncome: globalResults.income
  }
}

const mapStateToProps = state => {
  return {
    accountName: state.account.name,
    dashboardTiles: getGlobalMetrics(state.client.readings, state.account.accType),
    siteCards: getSites(state.client.readings, state.account.accType),
    accType: state.account.accType
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onMount: () => {
      const state = store.getState()
      dispatch(call('fetch-readings', { sessionId: state.account.sessionId }))
    }
  }
}

const ClientDashboardContainer = connect(mapStateToProps, mapDispatchToProps)(ClientDashboard)

export default ClientDashboardContainer
