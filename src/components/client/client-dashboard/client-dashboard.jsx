import React from 'react'
import history from '../../../history'
import DashboardTiles from './dashboard-tiles'
import SiteCardsWrapper from './site-cards-wrapper'

class ClientDashboard extends React.Component {
  constructor(props) {
    super(props)
  }
  
  componentDidMount() {
    if (this.props.siteCards.length === 1) history.replace('/client/sites/' + this.props.siteCards[0].siteId)
    else this.props.onMount()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.siteCards !== this.props.siteCards && this.props.siteCards.length === 1 && this.props.accType === 'user') {
      history.push('/client/sites/' + this.props.siteCards[0].siteId)
    }
  }

  render() {
    return (
      <div>
        <DashboardTiles dashboardTiles={this.props.dashboardTiles} />
        <SiteCardsWrapper selectSite={this.props.selectSite} siteCards={this.props.siteCards} />
      </div>
    )
  }
}

export default ClientDashboard