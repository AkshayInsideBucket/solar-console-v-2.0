import React from 'react'
import { Line } from 'react-chartjs-2'

function LineChart({ readings }) {
  var data = {
    labels: [],
    datasets: [
      {
        label: 'Power (kW)',
        backgroundColor: 'rgba(33, 150, 244, 0.2)',
        borderColor: 'rgba(33, 150, 244,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(33, 150, 244, 0.4)',
        hoverBorderColor: 'rgba(33, 150, 244,1)',
        pointRadius: 0,
        data: []
      }
    ]
  };
  var options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
  readings.forEach(function (obj) {
    const date = new Date(obj.time)
    const hours = date.getUTCHours()
    const minutes = date.getUTCMinutes()
    const timeString = hours + ':' + minutes
    data.labels.push(timeString)
    data.datasets[0].data.push(obj.power/1000)
  })
  return (
    <Line data={data} options={options} />
  )
}

const PowerLineChart = (props) => {
  return (
    <div className="site-chart">
      <LineChart readings={props.data} />
    </div>
  )
}

export default PowerLineChart