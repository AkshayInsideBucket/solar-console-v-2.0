import React from 'react'
import { Bar } from 'react-chartjs-2'

function BarChart({ readings }) {
  var data = {
    labels: [],
    datasets: [
      {
        label: 'Energy (kWh)',
        backgroundColor: 'rgba(33, 150, 244, 0.2)',
        borderColor: 'rgba(33, 150, 244,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(33, 150, 244, 0.4)',
        hoverBorderColor: 'rgba(33, 150, 244,1)',
        data: []
      }
    ]
  }
  var options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
  readings.forEach(function (obj) {
    const date = new Date(obj.day)
    const day = date.getDate()
    const month = date.getMonth() + 1
    const year = date.getFullYear().toString().substring(2)
    const dateString = day + '/' + (month < 10 ? ('0' + month) : month) + '/' + year
    data.labels.push(dateString)
    data.datasets[0].data.push(obj.energy)
  })
  return (
    <Bar data={data} options={options} />
  )
}

const EnergyBarChart = (props) => {
  return (
    <div className="site-chart">
      <BarChart readings={props.data} />
    </div>
  )
}

export default EnergyBarChart