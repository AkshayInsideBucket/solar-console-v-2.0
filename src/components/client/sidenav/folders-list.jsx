import React from 'react'
import history from '../../../history'

const FoldersList = (props) => {
  const urlLength = (url) => {
    var array = url.split('/')
    return array.length
  }

  const siteParams = (url) => {
    var array = url.split('/')
    return array[3]
  }

  const inverterParams = (url) => {
    var array = url.split('/')
    return array[4]
  }

  return (
    <div className="folders-list">
      <ul>
        <li>
          <a className={(props.url === '/client') ? "active" : ''} style={{ background: (props.url === '/client') ? '#aed581' : 'transparent' }} onClick={() => history.push('/client')}>
            <i className="material-icons">dashboard</i>
            Dashboard
          </a>
        </li>
        <li>
          <a className={(props.url === '/client/report') ? "active" : ''} style={{ background: (props.url === '/client/report') ? '#aed581' : 'transparent' }} onClick={() => history.push('/client/report')}>
            <i className="material-icons">description</i>
            Report
          </a>
        </li>
      </ul>
    </div >
  )
}

export default FoldersList