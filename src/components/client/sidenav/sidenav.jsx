import React from 'react'
import FoldersList from './folders-list'
import { Avatar, Button } from 'antd'
import { Router, Route } from 'react-router-dom';

class Sidenav extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="sidenav">
        <div className="sidenav-top-content">
          <p className="sidenav-title">Solar Console</p>
          <div className="user-info">
            <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
            <div className="avatar-text">
              <span>Welcome !</span>
              <span>{this.props.accountName}</span>
            </div>
          </div>
        </div>
        <FoldersList url={this.props.url} />
        <Button onClick={this.props.logout} type="primary" icon="logout">Logout</Button>
      </div>
    )
  }
}

export default Sidenav