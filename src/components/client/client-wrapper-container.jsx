import { connect } from 'react-redux'
import ClientWrapper from './client-wrapper'
import history from '../../history'
import store from '../../store'

const accountName = ['user1']

const mapDispatchToProps = (dispatch) => {
  return {
    onMount: () => {
      // Things to do on mount
    },
    logout: () => {
      history.replace('/')
      store.dispatch({ type: 'logout' })
    }
  }
}

const mapStateToProps = state => {
  return {
    accountName: accountName
  }
}

const ClientWrapperContainer = connect(mapStateToProps, mapDispatchToProps)(ClientWrapper)

export default ClientWrapperContainer
