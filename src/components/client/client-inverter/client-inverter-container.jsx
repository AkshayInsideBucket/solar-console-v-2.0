import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import ClientInverter from './client-inverter'
import store from '../../../store'
import history from '../../../history'
import { call } from '../../../reducers/client-api'

const getInverterData = (readings, inverterId, siteId) => {
  const index = readings.findIndex(reading => reading.site_id === siteId && reading.inverter_id === inverterId)
  if (index !== -1) {
    let result = Object.assign({}, readings.find(reading => reading.site_id === siteId && reading.inverter_id === inverterId))
    result.inverterId = result.inverter_id ? result.inverter_id : 0
    result.powerFactor = result.pfr ? result.pfr : 0
    result.capacity = result.capacity ? result.capacity : 0
    result.currentPower = result.apw ? result.apw : 0
    result.peakPower = result.pkw ? result.pkw : 0
    result.todayEnergy = result.etd ? result.etd : 0
    result.monthlyEnergy = result.myd ? result.myd : 0
    result.annualEnergy = result.ayd ? result.ayd : 0
    result.totalEnergy = result.tyd ? result.tyd : 0
    result.todayIncome = (result.etd * result.rate) ? (result.etd * result.rate) : 0
    result.monthlyIncome = (result.myd * result.rate) ? (result.myd * result.rate) : 0
    result.annualIncome = (result.ayd * result.rate) ? (result.ayd * result.rate) : 0
    result.totalIncome = (result.tyd * result.rate) ? (result.tyd * result.rate) : 0
    result.mpptShowLevel = result.mppt ? result.mppt : 1
    result.p1v = result.p1v ? (result.p1v / 10) : 0
    result.p2v = result.p2v ? (result.p2v / 10) : 0
    result.p3v = result.p3v ? (result.p3v / 10) : 0
    result.p1i = result.p1i ? (result.p1i / 100) : 0
    result.p2i = result.p2i ? (result.p2i / 100) : 0
    result.p3i = result.p3i ? (result.p3i / 100) : 0
    result.p1p = result.p1p ? (result.p1p / 1000) : 0
    result.p2p = result.p2p ? (result.p2p / 1000) : 0
    result.p3p = result.p3p ? (result.p3p / 1000) : 0
    return result
  } else {
    return { inverterId: '--', powerFactor: 0, capacity: 0, currentPower: 0, peakPower: 0, todayEnergy: 0, monthlyEnergy: 0, annualEnergy: 0, totalEnergy: 0, totalIncome: 0, annualIncome: 0, todayIncome: 0, mpptShowLevel: 1, p1v: '0', p1p: '0', p1i: '0' }
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onMount: () => {
      let state = store.getState()
      let today = new Date()
      let dd = today.getDate();
      let mm = today.getMonth() + 1
      const yyyy = today.getFullYear();
      if (dd < 10) dd = '0' + dd.toString()
      if (mm < 10) mm = '0' + mm.toString()
      const todayString = yyyy + '-' + mm + '-' + dd
      let dateString = today.getFullYear() + '-' + (today.getMonth() + 1) + '-01'
      dispatch(call('fetch-inverter-energy-chart', { sessionId: state.account.sessionId, siteId: ownProps.match.params.siteId, inverterId: ownProps.match.params.inverterId, firstDateOfMonth: dateString }))
      dispatch(call('fetch-inverter-power-chart', { sessionId: state.account.sessionId, siteId: ownProps.match.params.siteId, inverterId: ownProps.match.params.inverterId, date: todayString }))
      dispatch(call('fetch-readings', { sessionId: state.account.sessionId }))
    },
    energySet: (month, year) => {
      let state = store.getState()
      dispatch(call('fetch-inverter-energy-chart', { sessionId: state.account.sessionId, siteId: ownProps.match.params.siteId, inverterId: ownProps.match.params.inverterId, firstDateOfMonth: year + '-' + month + '-01' }))
    },
    powerSet: (date) => {
      let state = store.getState()
      dispatch(call('fetch-inverter-power-chart', { sessionId: state.account.sessionId, siteId: ownProps.match.params.siteId, inverterId: ownProps.match.params.inverterId, date: date }))
    }
  }
}


const mapStateToProps = (state, ownProps) => {
  return {
    accountName: state.account.name,
    inverterData: getInverterData(state.client.readings, ownProps.match.params.inverterId, ownProps.match.params.siteId),
    mpptData: [getInverterData(state.client.readings, ownProps.match.params.inverterId, ownProps.match.params.siteId)],
    energyChartData: state.client.charts.inverterEnergy,
    powerChartData: state.client.charts.inverterPower
  }
}

const ClientInverterContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientInverter))

export default ClientInverterContainer
