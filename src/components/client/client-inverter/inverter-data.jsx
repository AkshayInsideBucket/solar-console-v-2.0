import React from 'react'
import { Row, Col } from 'antd';

class InverterData extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timedOut: false
    }
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  tick() {
    const currentTime = new Date(new Date().toString())
    const lastTimeStamp = new Date(this.props.inverterData.time)
    if ((currentTime - lastTimeStamp) > 1800000) this.setState({ timedOut: true })
    else this.setState({ timedOut: false })
  }

  render() {
    return (
      <div className="inverter-data">
        <Row>
          <Col xs={{ span: 24, offset: 0 }} lg={{ span: 22, offset: 1 }}>
            <p className="tile-title">Power :</p>
            <div className="inverter-tile">
              <div className="tile-content power-tile">
                <p className="value color-1">{this.props.inverterData.inverterId}</p>
                <p className="description">Inverter ID</p>
              </div>
              <div className="tile-content power-tile">
                <p className="value color-2">{(this.props.inverterData.capacity/1000) + ' kW'}</p>
                <p className="description">Capacity</p>
              </div>
              <div className="tile-content power-tile">
                <p className="value color-3">{this.props.inverterData.powerFactor}</p>
                <p className="description">Power Factor</p>
              </div>
              <div className="tile-content power-tile">
                <p className="value color-4">{(this.props.inverterData.peakPower/1000) + ' kW'}</p>
                <p className="description">Peak Power</p>
              </div>
            </div>
            <p className="tile-title">Energy :</p>
            <div className="inverter-tile">
              <div className="tile-content energy-tile">
                <p className="value color-1">{(this.state.timedOut) ? '--' : ((this.props.inverterData.todayEnergy) + ' kWh')}</p>
                <p className="description">Today's Energy</p>
              </div>
              <div className="tile-content energy-tile">
                <p className="value color-2">{(this.props.inverterData.monthlyEnergy) + ' kWh'}</p>
                <p className="description">Monthly Energy</p>
              </div>
              <div className="tile-content energy-tile">
                <p className="value color-3">{(this.props.inverterData.annualEnergy) + ' kWh'}</p>
                <p className="description">Annual Energy</p>
              </div>
              <div className="tile-content energy-tile">
                <p className="value color-4">{(this.props.inverterData.totalEnergy) + ' kWh'}</p>
                <p className="description">Total Energy</p>
              </div>
            </div>
            <p className="tile-title">Income :</p>
            <div className="inverter-tile">
              <div className="tile-content income-tile">
                <p className="value color-1">{'Rs ' + (this.props.inverterData.todayIncome)}</p>
                <p className="description">Today's Income</p>
              </div>
              <div className="tile-content income-tile">
                <p className="value color-2">{'Rs ' + (this.props.inverterData.monthlyIncome)}</p>
                <p className="description">Monthly Income</p>
              </div>
              <div className="tile-content income-tile">
                <p className="value color-3">{'Rs ' + (this.props.inverterData.annualIncome)}</p>
                <p className="description">Annual Income</p>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default InverterData