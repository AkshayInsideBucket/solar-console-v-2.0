import React from 'react'
import { Row, Col, Table } from 'antd';

const MpptLevelTable = (props) => {
  let columns1 = []
  let columns2 = []
  let columns3 = []
  let column1_title
  let column2_title
  let column3_title

  if (props.inverterData.mpptShowLevel === 1) {
    columns1 = [
      {
        title: 'Voltage (V)',
        dataIndex: 'p1v',
        key: 'p1v'
      },
      {
        title: 'Power (kW)',
        dataIndex: 'p1p',
        key: 'p1p'
      },
      {
        title: 'Current (A)',
        dataIndex: 'p1i',
        key: 'p1i'
      }
    ]
    columns2 = []
    columns3 = []
    column1_title = 'Mppt 1 :'
  }


  if (props.inverterData.mpptShowLevel === 2) {
    columns1 = [
      {
        title: 'Voltage (V)',
        dataIndex: 'p1v',
        key: 'p1v'
      },
      {
        title: 'Power (kW)',
        dataIndex: 'p1p',
        key: 'p1p'
      },
      {
        title: 'Current (A)',
        dataIndex: 'p1i',
        key: 'p1i'
      }
    ]
    columns2 = [
      {
        title: 'Voltage (V)',
        dataIndex: 'p2v',
        key: 'p2v'
      },
      {
        title: 'Power (kW)',
        dataIndex: 'p2p',
        key: 'p2p'
      },
      {
        title: 'Current (A)',
        dataIndex: 'p2i',
        key: 'p2i'
      }
    ]
    columns3 = []
    column1_title = 'Mppt 1 :'
    column2_title = 'Mppt 2 :'
  }


  if (props.inverterData.mpptShowLevel === 3) {
    columns1 = [
      {
        title: 'Voltage (V)',
        dataIndex: 'p1v',
        key: 'p1v'
      },
      {
        title: 'Power (kW)',
        dataIndex: 'p1p',
        key: 'p1p'
      },
      {
        title: 'Current (A)',
        dataIndex: 'p1i',
        key: 'p1i'
      }
    ]
    columns2 = [
      {
        title: 'Voltage (V)',
        dataIndex: 'p2v',
        key: 'p2v'
      },
      {
        title: 'Power (kW)',
        dataIndex: 'p2p',
        key: 'p2p'
      },
      {
        title: 'Current (A)',
        dataIndex: 'p2i',
        key: 'p2i'
      }
    ]
    columns3 = [
      {
        title: 'Voltage (V)',
        dataIndex: 'p3v',
        key: 'p3v'
      },
      {
        title: 'Power (kW)',
        dataIndex: 'p3p',
        key: 'p3p'
      },
      {
        title: 'Current (A)',
        dataIndex: 'p3i',
        key: 'p3i'
      }
    ]
    column1_title = 'Mppt 1 :'
    column2_title = 'Mppt 2 :'
    column3_title = 'Mppt 3 :'
  }


  return (
    <div className="inverter-mppt-level">
      <Row>
        <Col xs={{ span: 24, offset: 0 }} md={{ span: 22, offset: 1 }} lg={{ span: 20, offset: 2 }}>
          <div style={{marginBottom:'24px'}}>
            <p className="mppt-title">{column1_title}</p>
            <Table columns={columns1} pagination={{ hideOnSinglePage: true }} dataSource={props.mpptData} />
          </div>
          <div style={{marginBottom:'24px'}}>
            <p className="mppt-title">{column2_title}</p>
            <Table columns={columns2} pagination={{ hideOnSinglePage: true }} dataSource={props.mpptData} />
          </div>
          <div style={{marginBottom:'0px'}}>
            <p className="mppt-title">{column3_title}</p>
            <Table columns={columns3} pagination={{ hideOnSinglePage: true }} dataSource={props.mpptData} />
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default MpptLevelTable