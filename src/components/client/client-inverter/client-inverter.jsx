import React from 'react'
import history from '../../../history'
import { Avatar, Button, Icon, Row, Col } from 'antd';
import InverterData from './inverter-data'
import InverterCharts from './inverter-charts';
import MpptLevelTable from './mppt-level-table'

class ClientInverter extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onMount()
  }

  render() {
    return (
      <div>
        <Row style={{ marginTop: '20px' }}>
          <Col xs={{ span: 22, offset: 1 }} md={{ span: 20, offset: 2 }} xl={{ span: 22, offset: 1 }}>
            <Button icon="arrow-left" type="primary" onClick={() => history.goBack()}>Back</Button>
          </Col>
        </Row>
        <InverterCharts inverterData={this.props.inverterData} powerChartData={this.props.powerChartData} energyChartData={this.props.energyChartData} powerSet={this.props.powerSet} energySet={this.props.energySet} />
        <InverterData inverterData={this.props.inverterData} />
        <MpptLevelTable inverterData={this.props.inverterData} mpptData={this.props.mpptData} />
      </div>
    )
  }
}

export default ClientInverter