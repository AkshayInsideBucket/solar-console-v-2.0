import React from 'react'

const FoldersList = (props) => {
  const onSelect = (id) => {
    props.selectLink(id)
    if (id == '1') {
      props.inverters();
    }
    if (id == '2') {
      props.report();
    }
  }

  return (
    <div className="folders-list">
      <ul>
        <li>
          <a className={(props.folderId === '1') ? "active" : ''} style={{ background: (props.folderId === '1') ? '#aed581' : 'transparent' }} onClick={() => onSelect('1')}>
            <i className="material-icons">battery_charging_full</i>
            Inverters
          </a>
        </li>
        <li>
          <a className={(props.folderId === '2') ? "active" : ''} style={{ background: (props.folderId === '2') ? '#aed581' : 'transparent' }} onClick={() => onSelect('2')}>
            <i className="material-icons">description</i>
            Report
          </a>
        </li>
      </ul>
    </div>
  )
}

export default FoldersList