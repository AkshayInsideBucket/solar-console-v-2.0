import React from 'react'

const ExtraTabContent = (props) => {
  return (
    <div className="extra-tab-content">
      <i className="material-icons power-icon">power</i>
      <div className="flex-content">
        <p className="value">{props.inverterData.currentPower/1000+' kW'}</p>
        <p className="description">Current Power</p>
      </div>
    </div>
  )
}

export default ExtraTabContent