import React from 'react'
import { Row, Col, Card, Tabs, Button } from 'antd'
import WrappedEnergyChart from './energy-chart'
import WrappedPowerChart from './power-chart'
import ExtraTabContent from './extra-tab-content'

const TabPane = Tabs.TabPane;

const InverterCharts = (props) => {
  return (
    <div className="inverter-charts">
      <Row>
        <Col xs={{ span: 24, offset: 0 }} md={{ span: 20, offset: 2 }}>
          <Card bordered={false}>
            <Tabs defaultActiveKey="1" tabBarExtraContent={<ExtraTabContent inverterData={props.inverterData} />}>
              <TabPane tab="Power" key="1">
                <WrappedPowerChart powerChartData={props.powerChartData} powerSet={props.powerSet} />
              </TabPane>
              <TabPane tab="Energy" key="2">
                <WrappedEnergyChart energyChartData={props.energyChartData} energySet={props.energySet} />
              </TabPane>
            </Tabs>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default InverterCharts