import React from 'react'
import { Row, Col, Card, Form, Select, Button, DatePicker } from 'antd'
import moment from 'moment';
import PowerLineChart from '../charts/line-chart'

const FormItem = Form.Item;
const Option = Select.Option;

class PowerChart extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        const values = {
          ...fieldsValue,
          'date': fieldsValue['date-picker'].format('YYYY-MM-DD'),
        }
        this.props.powerSet(values.date)
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const dateFormat = 'YYYY-MM-DD';
    let today = new Date()
    let dd = today.getDate();
    let mm = today.getMonth() + 1
    const yyyy = today.getFullYear();
    if (dd < 10) dd = '0' + dd
    if (mm < 10) mm = '0' + mm
    const todayString = yyyy + '-' + mm + '-' + dd
    return (
      <div className="power-chart">
        <PowerLineChart data={this.props.powerChartData} />
        <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col xs={{ span: 12, offset: 0 }} md={{ span: 8, offset: 0 }} xl={{ span: 6, offset: 0 }} style={{ paddingLeft: '32px' }}>
              <FormItem>
                {getFieldDecorator('date-picker', {
                  initialValue: moment(todayString, dateFormat),
                  rules: [
                    { type: 'object', required: true, message: 'Please select a date!' }
                  ]
                })(
                  <DatePicker format={dateFormat} />
                  )}
              </FormItem>
            </Col>
            <Col xs={{ span: 12, offset: 0 }} md={{ span: 8, offset: 0 }} xl={{ span: 6, offset: 0 }} style={{ paddingLeft: '32px' }}>
              <FormItem>
                <Button type="primary" htmlType="submit">Set</Button>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

const WrappedPowerChart = Form.create()(PowerChart);


export default WrappedPowerChart