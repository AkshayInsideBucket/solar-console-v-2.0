import React from 'react'
import { Row, Col, Card, Form, Select, Button } from 'antd'
import EnergyBarChart from '../charts/bar-chart'

const FormItem = Form.Item;
const Option = Select.Option;

class EnergyChart extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.energySet(values.month, values.year)
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    let last5Years = []
    for (let index = 0; index < 5; index++) {
      last5Years.push(new Date().getFullYear() - index)
    }
    let currentMonth = new Date().getMonth() + 1
    if (currentMonth < 10) currentMonth = '0' + currentMonth.toString()
    else currentMonth = currentMonth.toString()
    return (
      <div className="energy-chart">
        <EnergyBarChart data={this.props.energyChartData}/>
        <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 8, offset: 0 }} style={{ paddingLeft: '32px' }}>
              <FormItem>
                {getFieldDecorator('month', {
                  initialValue: currentMonth,
                  rules: [
                    { required: true, message: 'Please select a month!' }
                  ]
                })(
                  <Select placeholder="Select a month">
                    <Option value="01">January</Option>
                    <Option value="02">February</Option>
                    <Option value="03">March</Option>
                    <Option value="04">April</Option>
                    <Option value="05">May</Option>
                    <Option value="06">June</Option>
                    <Option value="07">July</Option>
                    <Option value="08">August</Option>
                    <Option value="09">September</Option>
                    <Option value="10">October</Option>
                    <Option value="11">November</Option>
                    <Option value="12">December</Option>
                  </Select>
                  )}
              </FormItem>
            </Col>
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 8, offset: 0 }} style={{ paddingLeft: '32px' }}>
              <FormItem>
                {getFieldDecorator('year', {
                   initialValue: last5Years[0],
                  rules: [
                    { required: true, message: 'Please select a year!' }
                  ]
                })(
                  <Select placeholder="Select a year">
                    {last5Years.map(year => <Option value={year}>{year}</Option>)}
                  </Select>
                  )}
              </FormItem>
            </Col>
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 8, offset: 0 }} style={{ paddingLeft: '32px' }}>
              <FormItem>
                <Button type="primary" htmlType="submit">Set</Button>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

const WrappedEnergyChart = Form.create()(EnergyChart);


export default WrappedEnergyChart