import { notification } from 'antd'

const CONNECT = 'client-api-connect';
const CONNECTED = 'client-api-connected';
const CLOSED = 'client-api-closed';
const CALL = 'client-api-call';

const SET_VALIDATION = 'client-api-set-validate';
const DEL_VALIDATION = 'client-api-del-validate';

const FETCH_JSON = 'client-api-fetch-json';

let api = { buffer: [] };

export const connect = (url, retry) => {
  return { type: CONNECT, url: url, retry: retry };
};

export const call = (event, data) => {
  let req = { event: event, data: data };
  console.log('Sending: ', req);
  return { type: CALL, payload: JSON.stringify(req) };
};

export const get = (event, url) => {
  return { type: FETCH_JSON, method: 'GET', event: event, url: url }
}

export const post = (event, url, obj) => {
  return { type: FETCH_JSON, method: 'POST', event: event, url: url, body: JSON.stringify(obj) }
}

export const put = (event, url, obj) => {
  return { type: FETCH_JSON, method: 'PUT', event: event, url: url, body: JSON.stringify(obj) }
}

export const del = (event, url) => {
  return { type: FETCH_JSON, method: 'DELETE', event: event, url: url }
}

export const setValidationAction = (action) => {
  return { type: SET_VALIDATION, action: action }
}

export const delValidationAction = () => {
  return { type: DEL_VALIDATION }
}

export const notificationMiddleware = store => next => action => {
  if (action.type.endsWith('-response') && action.data.notify) {
    let notify = action.data.notify;
    notification[notify.type]({ message: notify.title, description: notify.msg });
  }
  next(action);
};

export const clientApiMiddleware = store => next => action => {
  switch (action.type) {
    case CONNECT:
      let socket = new WebSocket(action.url);
      api.url = action.url;
      api.retry = action.retry;

      socket.onopen = function (e) {
        api.isConnected = true;
        api.socket = socket;
        next({ type: CONNECTED });
        api.buffer.forEach(payload => {
          store.dispatch({ type: CALL, payload: payload })
        })
      };

      socket.onmessage = function (e) {
        let res = JSON.parse(e.data);
        console.log('Received: ', res);
        next({ type: res.event + '-response', data: res.data, status: res.status });
      };

      socket.onclose = function (e) {
        if (api.retry) {
          setTimeout(() => store.dispatch({ type: CONNECT, url: api.url, retry: true }), 5000);
        }
        api.isConnected = false;
        next({ type: CLOSED });
      };
      break;

    case CALL:
      if (api.isConnected) api.socket.send(action.payload);
      else api.buffer.push(action.payload)
      break;

    case SET_VALIDATION:
      api.validationAction = action.action;
      break;

    case DEL_VALIDATION:
      api.validationAction = null;
      break;

    case FETCH_JSON:
      let options = {
        method: action.method, credentials: 'include', headers: new Headers({
          'Content-Type': 'application/json'
        })
      };
      if (action.method === 'POST' || action.method === 'PUT') {
        options.body = action.body;
      }

      let status = 0;
      fetch(action.url, options)
        .then((response) => {
          status = response.status
          if (response.status === 401 && api.validationAction) {
            store.dispatch(api.validationAction);
          }
          return response.text()
        })
        .then((res) => {
          console.log('Received String:', res);
          let data = JSON.parse(res);
          console.log('Received:', data);
          next({ type: action.event + '-response', data: data, status: { code: status } })
        })
        .catch((error) => { console.log('Fetch request failed: ', error); });
      break;

    default:
      break;

  }

  return next(action);
};