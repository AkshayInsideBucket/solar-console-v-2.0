import { combineReducers } from 'redux'
import { createReducer } from './reducers-middleware'

const account = combineReducers({
    sessionId: createReducer('account.sessionId', 'literal', ''),
    accType: createReducer('account.accType', 'literal', ''),
    name: createReducer('account.name', 'literal', '')
})

const admin = combineReducers({
    accounts: createReducer('admin.accounts', 'object-array', []),
    sites: createReducer('admin.sites', 'object-array', []),
    inverters: createReducer('admin.inverters', 'object-array', [])
})

const client = combineReducers({
    charts: combineReducers({
        inverterEnergy: createReducer('client.charts.inverterEnergy', 'object-array', []),
        inverterPower: createReducer('client.charts.inverterPower', 'object-array', []),
        siteEnergy: createReducer('client.charts.siteEnergy', 'object-array', []),
    }),
    sensorReadings: createReducer('client.sensorReadings', 'object-array', []),
    readings: createReducer('client.readings', 'object-array', []),
    reports: combineReducers({
        daily: combineReducers({
            overview: createReducer('client.reports.daily.overview', 'object', {capacity: 0, energy: 0, income: 0, peak: 0, mppt: 1}),
            table: createReducer('client.reports.daily.table', 'object-array', [])
        }),
        site: combineReducers({
            overview: createReducer('client.reports.site.overview', 'object', {capacity: 0, energy: 0, income: 0}),
            table: createReducer('client.reports.site.table', 'object-array', [])
        }),
        inverter: combineReducers({
            overview: createReducer('client.reports.inverter.overview', 'object', {capacity: 0, energy: 0, income: 0}),
            table: createReducer('client.reports.inverter.table', 'object-array', [])
        }),
        yearly: combineReducers({
            overview: createReducer('client.reports.yearly.overview', 'object', {capacity: 0, energy: 0, income: 0}),
            table: createReducer('client.reports.yearly.table', 'object-array', [])
        }),
        selectedSite: createReducer('client.reports.selectedSite', 'literal', ''),
        requested: createReducer('client.reports.requested', 'literal', false)
    })
})

const reducers = combineReducers({ account, admin, client });

export default reducers;