const config = {
  "login-response": [
    { type: 'account.sessionId', operation: 'set', value: 'data.sessionId', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] },
    { type: 'account.accType', operation: 'set', value: 'data.accType', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] },
    { type: 'account.name', operation: 'set', value: 'data.name', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] },
    { type: '---history', url: 'admin', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true, }, { operator: '===', operand1: 'data.accType', operand2: 'admin' }] },
    { type: '---history', url: 'client', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true, }, { operator: '===', operand1: 'data.accType', operand2: 'user' }] },
    { type: '---history', url: 'client', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true, }, { operator: '===', operand1: 'data.accType', operand2: 'master' }] },
   ],
  "fetch-readings-response": [
    { type: 'admin.accounts', operation: 'set', value: 'data.accounts', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true, }, { operator: '===', operand1: 'data.accType', operand2: 'admin' }] },
    { type: 'client.readings', operation: 'set', value: 'data.readings', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true, }, { operator: '===', operand1: 'data.accType', operand2: 'user' }] },
    { type: 'client.readings', operation: 'set', value: 'data.readings', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true, }, { operator: '===', operand1: 'data.accType', operand2: 'master' }] },
  ],
  "fetch-sensor-readings-response": [
    { type: 'client.sensorReadings', operation: 'set', value: 'data.result', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true, }] }
  ],
  "fetch-site-energy-chart-response": [
    { type: 'client.charts.siteEnergy', operation: 'set', value: 'data.result', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] },
    { type: 'client.charts.siteEnergy', operation: 'set', value: [], conditions: [{ operator: '===', operand1: 'data.ack', operand2: false }] }
  ],
  "fetch-inverter-energy-chart-response": [
    { type: 'client.charts.inverterEnergy', operation: 'set', value: 'data.result', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] },
    { type: 'client.charts.inverterEnergy', operation: 'set', value: [], conditions: [{ operator: '===', operand1: 'data.ack', operand2: false }] }
  ],
  "fetch-inverter-power-chart-response": [
    { type: 'client.charts.inverterPower', operation: 'set', value: 'data.result', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] },
    { type: 'client.charts.inverterPower', operation: 'set', value: [], conditions: [{ operator: '===', operand1: 'data.ack', operand2: false }] }
  ],
  "fetch-daily-report-response": [
    {type: 'client.reports.daily.overview', operation: 'set', value: 'data.overview', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.daily.table', operation: 'set', value: 'data.table', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.requested', operation: 'set', value: false }
  ],
  "fetch-site-report-response": [
    {type: 'client.reports.site.overview', operation: 'set', value: 'data.overview', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.site.table', operation: 'set', value: 'data.table', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.requested', operation: 'set', value: false }
  ],
  "fetch-inverter-report-response": [
    {type: 'client.reports.inverter.overview', operation: 'set', value: 'data.overview', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.inverter.table', operation: 'set', value: 'data.table', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.requested', operation: 'set', value: false }
  ],
  "fetch-yearly-report-response": [
    {type: 'client.reports.yearly.overview', operation: 'set', value: 'data.overview', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.yearly.table', operation: 'set', value: 'data.table', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }]},
    {type: 'client.reports.requested', operation: 'set', value: false }
  ],
  "add-user-response": [
    { type: 'admin.accounts', operation: 'append', value: 'data.user', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "update-user-response": [
    { type: 'admin.accounts', operation: 'updateByKey', key: 'account', value: 'data.account', updates: [{ key: 'name', value: 'data.name' }, { key: 'email', value: 'data.email' }, { key: 'pass', value: 'data.pass' }, { key: 'contact', value: 'data.contact' }], conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "remove-user-response": [
    { type: 'admin.accounts', operation: 'removeByKey', key: 'account', value: 'data.account', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "fetch-sites-response": [
    { type: 'admin.sites', operation: 'set', value: 'data.sites', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "add-site-response": [
    { type: 'admin.sites', operation: 'append', value: 'data.site', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "update-site-response": [
    { type: 'admin.sites', operation: 'updateByKey', key: 'site_id', value: 'data.siteId', updates: [{ key: 'name', value: 'data.name' }, { key: 'location', value: 'data.location' }, { key: 'rate', value: 'data.rate' }], conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "remove-site-response": [
    { type: 'admin.sites', operation: 'removeByKey', key: 'site_id', value: 'data.siteId', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "fetch-inverters-response": [
    { type: 'admin.inverters', operation: 'set', value: 'data.inverters', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "add-inverter-response": [
    { type: 'admin.inverters', operation: 'append', value: 'data.inverter', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "update-inverter-response": [
    { type: 'admin.inverters', operation: 'updateByKey', key: 'inverter_id', value: 'data.inverterId', updates: [{ key: 'capacity', value: 'data.capacity' }, { key: 'mppt', value: 'data.mppt' }], conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ],
  "remove-inverter-response": [
    { type: 'admin.inverters', operation: 'removeByKey', key: 'inverter_id', value: 'data.inverterId', conditions: [{ operator: '===', operand1: 'data.ack', operand2: true }] }
  ]
}
export default config