import { call, get, post, put, del } from '../client-api'
export const createReducer = (name, type, initialState) => {
  switch (type) {
    case 'literal':
      return (state = initialState, action) => {

        if (action.type === name && checkConditions(action)) {
          let nextState
          switch (action.operation) {
            case 'set':
              nextState = fetchValue(action, 'value')
              if (nextState === undefined) return state
              else return nextState

            default:
              return state
          }
        } else if (action.type === 'logout') return initialState
        else return state
      }

    case 'literal-array':
      return (state = initialState, action) => {
        if (action.type === name && checkConditions(action)) {
          let nextState
          switch (action.operation) {
            case 'set':
              nextState = fetchValue(action, 'value')
              if (!nextState || !(nextState.constructor === Array)) return state
              else return nextState

            case 'append':
              return appendValue(state, action, 'literal')
            case 'prepend':
              return prependValue(state, action, 'literal')
            case 'removeByValue':
              return removeByValue(state, action)
            case 'removeByIndex':
              return removeByIndex(state, action)

            default:
              return state
          }
        } else if (action.type === 'logout') return initialState
        else return state
      }

    case 'object':
      return (state = initialState, action) => {
        if (action.type === name && checkConditions(action)) {
          let nextState
          switch (action.operation) {
            case 'set':
              nextState = fetchValue(action, 'value')
              if (!nextState) return state
              else return nextState

            default:
              return state
          }
        } else if (action.type === 'logout') return initialState
        else return state
      }

    case 'object-array':
      return (state = initialState, action) => {
        if (action.type === name && checkConditions(action)) {
          let nextState
          switch (action.operation) {
            case 'set':
              nextState = fetchValue(action, 'value')
              if (!nextState || !(nextState.constructor === Array)) return state
              else return nextState

            case 'append':
              return appendValue(state, action, 'object')
            case 'prepend':
              return prependValue(state, action, 'object')
            case 'removeByKey':
              return removeByKey(state, action)
            case 'removeByIndex':
              return removeByIndex(state, action)
            case 'updateByKey':
              return updateByKey(state, action)

            default:
              return state
          }
        } else if (action.type === 'logout') return initialState
        else return state
      }

    default:
      break;
  }

}

// Returns obj[key]
// If obj[key] is some address within obj, it fetches value from that address
// Examples:
// 1. obj = { value: 3 }, key = 'value' return 3
// 2. obj = { value: 'data.ack', data: { ack: true } }, key = 'value' return true

const fetchValue = function (obj, key) {
  return (typeof obj[key] === 'string' && obj[key].includes('.')) ? loadValue(obj, obj[key].split('.')) : obj[key]
}

const appendValue = function (state, action, type) {
  let value = fetchValue(action, 'value')
  if (value === undefined) return state
  if ((typeof value === 'object' && type === 'literal') || (typeof value !== 'object' && type === 'object')) return state
  let nextState = state.slice()
  nextState.push(value)
  return nextState
}

const prependValue = function (state, action, type) {
  let value = fetchValue(action, 'value')
  if (value === undefined) return state
  if ((typeof value === 'object' && type === 'literal') || (typeof value !== 'object' && type === 'object')) return state
  let nextState = state.slice()
  nextState.unshift(value)
  return nextState
}

const removeByValue = function (state, action) {
  let value = fetchValue(action, 'value')
  if (value === undefined) return state
  return state.filter(item => item !== action.value)
}

const removeByIndex = function (state, action) {
  if (action.index === undefined) return state
  if (action.index < 0 || action.index >= state.length) return state
  let nextState = state.slice()
  nextState.splice(action.index, 1)
  return nextState
}

const removeByKey = function (state, action) {
  const value = fetchValue(action, 'value')
  if (action.key === undefined || value === undefined || typeof action.key !== 'string') return state
  return state.filter(obj => obj[action.key] !== value)
}
const updateByKey = function (state, action) {
  const value = fetchValue(action, 'value')
  if (action.key === undefined || value === undefined || typeof action.key !== 'string') return state
  if (!action.updates || action.updates.constructor !== Array || action.updates.length === 0) return state
  return state.map(obj => {
    if (obj[action.key] !== value) return obj
    action.updates.forEach(update => {
      const value = (typeof update.value === 'string' && update.value.includes('.')) ? loadValue(action, update.value.split('.')) : update.value
      if (update.key && value) obj[update.key] = value
    })
    return obj
  })
}
// Loads value from a object
const loadValue = function (obj, keys) {
  return keys.reduce((prev, curr) => { return prev !== undefined ? (prev[curr] !== undefined ? prev[curr] : undefined) : undefined }, obj)
}

const checkConditions = function (action) {
  let result = true
  if (action.conditions) {
    action.conditions.forEach(condition => {
      let operand1 = (typeof condition.operand1 === 'string' && condition.operand1.includes('.')) ? loadValue(action, condition.operand1.split('.')) : condition.operand1
      let operand2 = (typeof condition.operand2 === 'string' && condition.operand2.includes('.')) ? fetchValue(action, condition.operand2.split('.')) : condition.operand2
      if (!checkCondition(condition.operator, operand1, operand2)) result = false
    })
  }

  return result
}

const checkCondition = function (operator, operator1, operator2) {
  if (operator === undefined || operator === null) return false
  switch (operator) {
    case '===': return operator1 === operator2
    case '!==': return operator1 !== operator2
    case '>=': return operator1 >= operator2
    case '<=': return operator1 <= operator2
    case '>': return operator1 > operator2
    case '<': return operator1 < operator2
    default:
      console.log('Check condition failed - got wrong operator: ', operator)
      return false
  }
}

export const initReducerMiddleware = (config, history) => {
  if (config === undefined) return store => next => action => next(action)
  return store => next => action => {
    let actions = config[action.type]
    if (actions !== undefined && actions.constructor === Array && actions.length > 0) {

      actions.forEach(obj => {
        if (obj.type && obj.type.startsWith('---')) {
          switch (obj.type) {
            case '---history':
              if (obj.url !== undefined && typeof obj.url === 'string' && checkConditions(Object.assign({}, action, { conditions: obj.conditions }))) {
                const params = obj.url.split('/')
                let finalUrl = ''
                let changeHistory = true
                params.forEach(param => {
                  if (param.startsWith(':')) {
                    if (param.includes('.')) {
                      const temp = loadValue(action, param.substring(1).split('.'))
                      if (temp !== undefined && temp !== null && typeof temp === 'string') finalUrl = finalUrl + '/' + temp
                      else changeHistory = false
                    }
                    else finalUrl = finalUrl + '/' + action[param.substring(1)]
                  }
                  else finalUrl += finalUrl + '/' + param
                })
                if (changeHistory) history.push(finalUrl)
              }
              break;

            case '---post':
              if (obj.event !== undefined && obj.url !== undefined && obj.body !== undefined && checkConditions(Object.assign({}, action, { conditions: obj.conditions }))) store.dispatch(post(obj.event, obj.url, obj.body))
              break;

            case '---put':
              if (obj.event !== undefined && obj.url !== undefined && obj.body !== undefined && checkConditions(Object.assign({}, action, { conditions: obj.conditions }))) store.dispatch(put(obj.event, obj.url, obj.body))
              break;

            case '---get':
              if (obj.event !== undefined && obj.url !== undefined && checkConditions(Object.assign({}, action, { conditions: obj.conditions }))) store.dispatch(get(obj.event, obj.url))
              break;

            case '---del':
              if (obj.event !== undefined && obj.url !== undefined && checkConditions(Object.assign({}, action, { conditions: obj.conditions }))) store.dispatch(del(obj.event, obj.url, obj.body))
              break;

            case '---call':
              if (obj.event !== undefined && obj.data !== undefined && checkConditions(Object.assign({}, action, { conditions: obj.conditions }))) store.dispatch(call(obj.event, obj.data))
              break;
            default:
              return next(Object.assign({}, obj, { data: action.data }))
              break;
          }
        }
        else return next(Object.assign({}, action, obj))
      })
    }
    else return next(action);
    return next(action)
  }
}