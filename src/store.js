import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import config from './reducers/config'
import { clientApiMiddleware, notificationMiddleware } from './reducers/client-api'
import { initReducerMiddleware } from './reducers/reducers-middleware'
import reducers from './reducers'
import history from './history'
import { loadState, saveState } from './localStorage'
import throttle from 'lodash/throttle'

const persistedState = loadState()
const store = createStore(reducers, persistedState, composeWithDevTools(applyMiddleware(clientApiMiddleware, initReducerMiddleware(config, history), notificationMiddleware)));

store.subscribe(throttle(() => {
  saveState(store.getState())
}, 1000))
export default store

